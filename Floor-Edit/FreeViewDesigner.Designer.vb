﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FreeViewDesigner
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnWidgetRemove = New System.Windows.Forms.Button()
        Me.lbWidgets = New System.Windows.Forms.ListBox()
        Me.gbProject = New System.Windows.Forms.GroupBox()
        Me.lbProjViews = New System.Windows.Forms.ListBox()
        Me.btnProjSetOffset = New System.Windows.Forms.Button()
        Me.txtProjPath = New System.Windows.Forms.TextBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.btnProjFirstView = New System.Windows.Forms.Button()
        Me.txtProjLoadOffset = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.pbProject = New System.Windows.Forms.PictureBox()
        Me.txtProjFirstView = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtProjName = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.gbView = New System.Windows.Forms.GroupBox()
        Me.txtViewCropSize = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnWidgetCancelSelection = New System.Windows.Forms.Button()
        Me.cbViewWidgetType = New System.Windows.Forms.ComboBox()
        Me.btnViewDrawWidget = New System.Windows.Forms.Button()
        Me.lblViewPanoramaLength = New System.Windows.Forms.Label()
        Me.lblViewLength = New System.Windows.Forms.Label()
        Me.lblViewPanoramaHeight = New System.Windows.Forms.Label()
        Me.lblViewHeight = New System.Windows.Forms.Label()
        Me.lblViewWidth = New System.Windows.Forms.Label()
        Me.lblViewWidthRatio = New System.Windows.Forms.Label()
        Me.lblViewHeightRatio = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtViewOffset = New System.Windows.Forms.TextBox()
        Me.gbWidget = New System.Windows.Forms.GroupBox()
        Me.btnWidgetSetOffset = New System.Windows.Forms.Button()
        Me.btnWidgetTargetView = New System.Windows.Forms.Button()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtWidgetViewOffset = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtWidgetTargetView = New System.Windows.Forms.TextBox()
        Me.pbWidget = New System.Windows.Forms.PictureBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtWidgetName = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cbWidgetType = New System.Windows.Forms.ComboBox()
        Me.txtWidgetHeight = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtWidgetTop = New System.Windows.Forms.TextBox()
        Me.txtWidgetLeft = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtWidgetWidth = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtViewName = New System.Windows.Forms.Label()
        Me.pbView = New System.Windows.Forms.PictureBox()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.LelToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.mbtnProjNew = New System.Windows.Forms.ToolStripMenuItem()
        Me.mbtnProjOpen = New System.Windows.Forms.ToolStripMenuItem()
        Me.mbtnProjSave = New System.Windows.Forms.ToolStripMenuItem()
        Me.mbtnProjClose = New System.Windows.Forms.ToolStripMenuItem()
        Me.mbtnProjExit = New System.Windows.Forms.ToolStripMenuItem()
        Me.toolstripitem = New System.Windows.Forms.ToolStripMenuItem()
        Me.mbtnViewNew = New System.Windows.Forms.ToolStripMenuItem()
        Me.mbtnViewOpen = New System.Windows.Forms.ToolStripMenuItem()
        Me.mbtnViewSave = New System.Windows.Forms.ToolStripMenuItem()
        Me.mbtnViewClose = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.mbtnHelpAbout = New System.Windows.Forms.ToolStripMenuItem()
        Me.gbProject.SuspendLayout()
        CType(Me.pbProject, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbView.SuspendLayout()
        Me.gbWidget.SuspendLayout()
        CType(Me.pbWidget, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnWidgetRemove
        '
        Me.btnWidgetRemove.Location = New System.Drawing.Point(640, 592)
        Me.btnWidgetRemove.Name = "btnWidgetRemove"
        Me.btnWidgetRemove.Size = New System.Drawing.Size(161, 23)
        Me.btnWidgetRemove.TabIndex = 24
        Me.btnWidgetRemove.Text = "Remove Widget"
        Me.btnWidgetRemove.UseVisualStyleBackColor = True
        '
        'lbWidgets
        '
        Me.lbWidgets.FormattingEnabled = True
        Me.lbWidgets.Location = New System.Drawing.Point(640, 447)
        Me.lbWidgets.Name = "lbWidgets"
        Me.lbWidgets.Size = New System.Drawing.Size(318, 134)
        Me.lbWidgets.TabIndex = 23
        '
        'gbProject
        '
        Me.gbProject.Controls.Add(Me.lbProjViews)
        Me.gbProject.Controls.Add(Me.btnProjSetOffset)
        Me.gbProject.Controls.Add(Me.txtProjPath)
        Me.gbProject.Controls.Add(Me.Label24)
        Me.gbProject.Controls.Add(Me.btnProjFirstView)
        Me.gbProject.Controls.Add(Me.txtProjLoadOffset)
        Me.gbProject.Controls.Add(Me.Label23)
        Me.gbProject.Controls.Add(Me.pbProject)
        Me.gbProject.Controls.Add(Me.txtProjFirstView)
        Me.gbProject.Controls.Add(Me.Label2)
        Me.gbProject.Controls.Add(Me.txtProjName)
        Me.gbProject.Controls.Add(Me.Label1)
        Me.gbProject.Location = New System.Drawing.Point(7, 47)
        Me.gbProject.Name = "gbProject"
        Me.gbProject.Size = New System.Drawing.Size(291, 626)
        Me.gbProject.TabIndex = 25
        Me.gbProject.TabStop = False
        Me.gbProject.Text = "Project"
        '
        'lbProjViews
        '
        Me.lbProjViews.FormattingEnabled = True
        Me.lbProjViews.Location = New System.Drawing.Point(14, 339)
        Me.lbProjViews.Name = "lbProjViews"
        Me.lbProjViews.Size = New System.Drawing.Size(271, 173)
        Me.lbProjViews.TabIndex = 61
        '
        'btnProjSetOffset
        '
        Me.btnProjSetOffset.Location = New System.Drawing.Point(136, 102)
        Me.btnProjSetOffset.Name = "btnProjSetOffset"
        Me.btnProjSetOffset.Size = New System.Drawing.Size(149, 23)
        Me.btnProjSetOffset.TabIndex = 60
        Me.btnProjSetOffset.Text = "Set current offset as load"
        Me.btnProjSetOffset.UseVisualStyleBackColor = True
        '
        'txtProjPath
        '
        Me.txtProjPath.Location = New System.Drawing.Point(73, 23)
        Me.txtProjPath.Name = "txtProjPath"
        Me.txtProjPath.ReadOnly = True
        Me.txtProjPath.Size = New System.Drawing.Size(212, 20)
        Me.txtProjPath.TabIndex = 59
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(11, 26)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(32, 13)
        Me.Label24.TabIndex = 58
        Me.Label24.Text = "Path:"
        '
        'btnProjFirstView
        '
        Me.btnProjFirstView.Location = New System.Drawing.Point(247, 71)
        Me.btnProjFirstView.Name = "btnProjFirstView"
        Me.btnProjFirstView.Size = New System.Drawing.Size(38, 23)
        Me.btnProjFirstView.TabIndex = 57
        Me.btnProjFirstView.Text = "..."
        Me.btnProjFirstView.UseVisualStyleBackColor = True
        '
        'txtProjLoadOffset
        '
        Me.txtProjLoadOffset.Location = New System.Drawing.Point(73, 104)
        Me.txtProjLoadOffset.Name = "txtProjLoadOffset"
        Me.txtProjLoadOffset.ReadOnly = True
        Me.txtProjLoadOffset.Size = New System.Drawing.Size(57, 20)
        Me.txtProjLoadOffset.TabIndex = 12
        '
        'Label23
        '
        Me.Label23.Location = New System.Drawing.Point(11, 101)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(56, 31)
        Me.Label23.TabIndex = 11
        Me.Label23.Text = "Load Offset:"
        '
        'pbProject
        '
        Me.pbProject.Location = New System.Drawing.Point(14, 156)
        Me.pbProject.Name = "pbProject"
        Me.pbProject.Size = New System.Drawing.Size(271, 167)
        Me.pbProject.TabIndex = 4
        Me.pbProject.TabStop = False
        '
        'txtProjFirstView
        '
        Me.txtProjFirstView.Location = New System.Drawing.Point(73, 73)
        Me.txtProjFirstView.Name = "txtProjFirstView"
        Me.txtProjFirstView.ReadOnly = True
        Me.txtProjFirstView.Size = New System.Drawing.Size(168, 20)
        Me.txtProjFirstView.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(11, 76)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(56, 15)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "First View:"
        '
        'txtProjName
        '
        Me.txtProjName.Location = New System.Drawing.Point(73, 47)
        Me.txtProjName.Name = "txtProjName"
        Me.txtProjName.ReadOnly = True
        Me.txtProjName.Size = New System.Drawing.Size(212, 20)
        Me.txtProjName.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(11, 50)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(38, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Name:"
        '
        'gbView
        '
        Me.gbView.Controls.Add(Me.txtViewCropSize)
        Me.gbView.Controls.Add(Me.Label3)
        Me.gbView.Controls.Add(Me.btnWidgetCancelSelection)
        Me.gbView.Controls.Add(Me.cbViewWidgetType)
        Me.gbView.Controls.Add(Me.btnViewDrawWidget)
        Me.gbView.Controls.Add(Me.lblViewPanoramaLength)
        Me.gbView.Controls.Add(Me.lblViewLength)
        Me.gbView.Controls.Add(Me.lblViewPanoramaHeight)
        Me.gbView.Controls.Add(Me.lblViewHeight)
        Me.gbView.Controls.Add(Me.lblViewWidth)
        Me.gbView.Controls.Add(Me.lblViewWidthRatio)
        Me.gbView.Controls.Add(Me.lblViewHeightRatio)
        Me.gbView.Controls.Add(Me.Label15)
        Me.gbView.Controls.Add(Me.txtViewOffset)
        Me.gbView.Controls.Add(Me.gbWidget)
        Me.gbView.Controls.Add(Me.txtViewName)
        Me.gbView.Controls.Add(Me.pbView)
        Me.gbView.Controls.Add(Me.lbWidgets)
        Me.gbView.Controls.Add(Me.btnWidgetRemove)
        Me.gbView.Location = New System.Drawing.Point(315, 47)
        Me.gbView.Name = "gbView"
        Me.gbView.Size = New System.Drawing.Size(989, 626)
        Me.gbView.TabIndex = 26
        Me.gbView.TabStop = False
        Me.gbView.Text = "View"
        '
        'txtViewCropSize
        '
        Me.txtViewCropSize.Location = New System.Drawing.Point(187, 389)
        Me.txtViewCropSize.Name = "txtViewCropSize"
        Me.txtViewCropSize.Size = New System.Drawing.Size(64, 20)
        Me.txtViewCropSize.TabIndex = 61
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(126, 392)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(55, 13)
        Me.Label3.TabIndex = 60
        Me.Label3.Text = "Crop Size:"
        '
        'btnWidgetCancelSelection
        '
        Me.btnWidgetCancelSelection.Location = New System.Drawing.Point(807, 592)
        Me.btnWidgetCancelSelection.Name = "btnWidgetCancelSelection"
        Me.btnWidgetCancelSelection.Size = New System.Drawing.Size(151, 23)
        Me.btnWidgetCancelSelection.TabIndex = 59
        Me.btnWidgetCancelSelection.Text = "Cancel Selection"
        Me.btnWidgetCancelSelection.UseVisualStyleBackColor = True
        '
        'cbViewWidgetType
        '
        Me.cbViewWidgetType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbViewWidgetType.FormattingEnabled = True
        Me.cbViewWidgetType.Items.AddRange(New Object() {"PassageWidget", "ZoomWidget", "PowerWidget", "ReturnWidget"})
        Me.cbViewWidgetType.Location = New System.Drawing.Point(349, 392)
        Me.cbViewWidgetType.Name = "cbViewWidgetType"
        Me.cbViewWidgetType.Size = New System.Drawing.Size(273, 21)
        Me.cbViewWidgetType.TabIndex = 57
        '
        'btnViewDrawWidget
        '
        Me.btnViewDrawWidget.Location = New System.Drawing.Point(349, 429)
        Me.btnViewDrawWidget.Name = "btnViewDrawWidget"
        Me.btnViewDrawWidget.Size = New System.Drawing.Size(273, 83)
        Me.btnViewDrawWidget.TabIndex = 56
        Me.btnViewDrawWidget.Text = "Draw Widget"
        Me.btnViewDrawWidget.UseVisualStyleBackColor = True
        '
        'lblViewPanoramaLength
        '
        Me.lblViewPanoramaLength.Location = New System.Drawing.Point(151, 451)
        Me.lblViewPanoramaLength.Name = "lblViewPanoramaLength"
        Me.lblViewPanoramaLength.Size = New System.Drawing.Size(136, 13)
        Me.lblViewPanoramaLength.TabIndex = 55
        Me.lblViewPanoramaLength.Text = "Panorama Length: XXXXX"
        '
        'lblViewLength
        '
        Me.lblViewLength.Location = New System.Drawing.Point(202, 421)
        Me.lblViewLength.Name = "lblViewLength"
        Me.lblViewLength.Size = New System.Drawing.Size(84, 13)
        Me.lblViewLength.TabIndex = 54
        Me.lblViewLength.Text = "Length: XXXXX"
        '
        'lblViewPanoramaHeight
        '
        Me.lblViewPanoramaHeight.Location = New System.Drawing.Point(9, 451)
        Me.lblViewPanoramaHeight.Name = "lblViewPanoramaHeight"
        Me.lblViewPanoramaHeight.Size = New System.Drawing.Size(136, 13)
        Me.lblViewPanoramaHeight.TabIndex = 53
        Me.lblViewPanoramaHeight.Text = "Panorama Height: XXXXX"
        '
        'lblViewHeight
        '
        Me.lblViewHeight.Location = New System.Drawing.Point(109, 421)
        Me.lblViewHeight.Name = "lblViewHeight"
        Me.lblViewHeight.Size = New System.Drawing.Size(114, 13)
        Me.lblViewHeight.TabIndex = 51
        Me.lblViewHeight.Text = "Height: XXXXX"
        '
        'lblViewWidth
        '
        Me.lblViewWidth.Location = New System.Drawing.Point(9, 421)
        Me.lblViewWidth.Name = "lblViewWidth"
        Me.lblViewWidth.Size = New System.Drawing.Size(94, 13)
        Me.lblViewWidth.TabIndex = 50
        Me.lblViewWidth.Text = "Width: XXXXX"
        '
        'lblViewWidthRatio
        '
        Me.lblViewWidthRatio.Location = New System.Drawing.Point(134, 480)
        Me.lblViewWidthRatio.Name = "lblViewWidthRatio"
        Me.lblViewWidthRatio.Size = New System.Drawing.Size(109, 13)
        Me.lblViewWidthRatio.TabIndex = 49
        Me.lblViewWidthRatio.Text = "Width Ratio: XXXXX"
        '
        'lblViewHeightRatio
        '
        Me.lblViewHeightRatio.Location = New System.Drawing.Point(9, 480)
        Me.lblViewHeightRatio.Name = "lblViewHeightRatio"
        Me.lblViewHeightRatio.Size = New System.Drawing.Size(119, 13)
        Me.lblViewHeightRatio.TabIndex = 48
        Me.lblViewHeightRatio.Text = "Height Ratio: XXXXX"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(9, 392)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(38, 13)
        Me.Label15.TabIndex = 46
        Me.Label15.Text = "Offset:"
        '
        'txtViewOffset
        '
        Me.txtViewOffset.Location = New System.Drawing.Point(53, 389)
        Me.txtViewOffset.Name = "txtViewOffset"
        Me.txtViewOffset.ReadOnly = True
        Me.txtViewOffset.Size = New System.Drawing.Size(56, 20)
        Me.txtViewOffset.TabIndex = 45
        '
        'gbWidget
        '
        Me.gbWidget.Controls.Add(Me.btnWidgetSetOffset)
        Me.gbWidget.Controls.Add(Me.btnWidgetTargetView)
        Me.gbWidget.Controls.Add(Me.Label14)
        Me.gbWidget.Controls.Add(Me.txtWidgetViewOffset)
        Me.gbWidget.Controls.Add(Me.Label13)
        Me.gbWidget.Controls.Add(Me.txtWidgetTargetView)
        Me.gbWidget.Controls.Add(Me.pbWidget)
        Me.gbWidget.Controls.Add(Me.Label12)
        Me.gbWidget.Controls.Add(Me.txtWidgetName)
        Me.gbWidget.Controls.Add(Me.Label7)
        Me.gbWidget.Controls.Add(Me.cbWidgetType)
        Me.gbWidget.Controls.Add(Me.txtWidgetHeight)
        Me.gbWidget.Controls.Add(Me.Label8)
        Me.gbWidget.Controls.Add(Me.txtWidgetTop)
        Me.gbWidget.Controls.Add(Me.txtWidgetLeft)
        Me.gbWidget.Controls.Add(Me.Label10)
        Me.gbWidget.Controls.Add(Me.Label9)
        Me.gbWidget.Controls.Add(Me.txtWidgetWidth)
        Me.gbWidget.Controls.Add(Me.Label11)
        Me.gbWidget.Location = New System.Drawing.Point(629, 19)
        Me.gbWidget.Name = "gbWidget"
        Me.gbWidget.Size = New System.Drawing.Size(342, 415)
        Me.gbWidget.TabIndex = 44
        Me.gbWidget.TabStop = False
        Me.gbWidget.Text = "Widget"
        '
        'btnWidgetSetOffset
        '
        Me.btnWidgetSetOffset.Location = New System.Drawing.Point(178, 172)
        Me.btnWidgetSetOffset.Name = "btnWidgetSetOffset"
        Me.btnWidgetSetOffset.Size = New System.Drawing.Size(149, 23)
        Me.btnWidgetSetOffset.TabIndex = 59
        Me.btnWidgetSetOffset.Text = "Set current offset as load"
        Me.btnWidgetSetOffset.UseVisualStyleBackColor = True
        '
        'btnWidgetTargetView
        '
        Me.btnWidgetTargetView.Location = New System.Drawing.Point(289, 146)
        Me.btnWidgetTargetView.Name = "btnWidgetTargetView"
        Me.btnWidgetTargetView.Size = New System.Drawing.Size(38, 23)
        Me.btnWidgetTargetView.TabIndex = 58
        Me.btnWidgetTargetView.Text = "..."
        Me.btnWidgetTargetView.UseVisualStyleBackColor = True
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(6, 177)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(91, 13)
        Me.Label14.TabIndex = 48
        Me.Label14.Text = "View Load Offset:"
        '
        'txtWidgetViewOffset
        '
        Me.txtWidgetViewOffset.Location = New System.Drawing.Point(103, 174)
        Me.txtWidgetViewOffset.Name = "txtWidgetViewOffset"
        Me.txtWidgetViewOffset.ReadOnly = True
        Me.txtWidgetViewOffset.Size = New System.Drawing.Size(69, 20)
        Me.txtWidgetViewOffset.TabIndex = 49
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(6, 151)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(67, 13)
        Me.Label13.TabIndex = 46
        Me.Label13.Text = "Target View:"
        '
        'txtWidgetTargetView
        '
        Me.txtWidgetTargetView.Location = New System.Drawing.Point(79, 148)
        Me.txtWidgetTargetView.Name = "txtWidgetTargetView"
        Me.txtWidgetTargetView.ReadOnly = True
        Me.txtWidgetTargetView.Size = New System.Drawing.Size(204, 20)
        Me.txtWidgetTargetView.TabIndex = 47
        '
        'pbWidget
        '
        Me.pbWidget.Location = New System.Drawing.Point(11, 205)
        Me.pbWidget.Name = "pbWidget"
        Me.pbWidget.Size = New System.Drawing.Size(318, 195)
        Me.pbWidget.TabIndex = 45
        Me.pbWidget.TabStop = False
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(6, 27)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(38, 13)
        Me.Label12.TabIndex = 42
        Me.Label12.Text = "Name:"
        '
        'txtWidgetName
        '
        Me.txtWidgetName.Location = New System.Drawing.Point(68, 24)
        Me.txtWidgetName.Name = "txtWidgetName"
        Me.txtWidgetName.Size = New System.Drawing.Size(259, 20)
        Me.txtWidgetName.TabIndex = 43
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(6, 53)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(34, 13)
        Me.Label7.TabIndex = 31
        Me.Label7.Text = "Type:"
        '
        'cbWidgetType
        '
        Me.cbWidgetType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbWidgetType.Enabled = False
        Me.cbWidgetType.FormattingEnabled = True
        Me.cbWidgetType.Items.AddRange(New Object() {"Passage Widget", "Power Widget", "Zoom Widget", "Return Widget"})
        Me.cbWidgetType.Location = New System.Drawing.Point(68, 50)
        Me.cbWidgetType.Name = "cbWidgetType"
        Me.cbWidgetType.Size = New System.Drawing.Size(259, 21)
        Me.cbWidgetType.TabIndex = 32
        '
        'txtWidgetHeight
        '
        Me.txtWidgetHeight.Location = New System.Drawing.Point(222, 106)
        Me.txtWidgetHeight.Name = "txtWidgetHeight"
        Me.txtWidgetHeight.Size = New System.Drawing.Size(105, 20)
        Me.txtWidgetHeight.TabIndex = 41
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(6, 83)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(28, 13)
        Me.Label8.TabIndex = 33
        Me.Label8.Text = "Left:"
        '
        'txtWidgetTop
        '
        Me.txtWidgetTop.Location = New System.Drawing.Point(222, 80)
        Me.txtWidgetTop.Name = "txtWidgetTop"
        Me.txtWidgetTop.Size = New System.Drawing.Size(105, 20)
        Me.txtWidgetTop.TabIndex = 40
        '
        'txtWidgetLeft
        '
        Me.txtWidgetLeft.Location = New System.Drawing.Point(44, 80)
        Me.txtWidgetLeft.Name = "txtWidgetLeft"
        Me.txtWidgetLeft.Size = New System.Drawing.Size(105, 20)
        Me.txtWidgetLeft.TabIndex = 34
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(175, 109)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(41, 13)
        Me.Label10.TabIndex = 39
        Me.Label10.Text = "Height:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(175, 83)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(29, 13)
        Me.Label9.TabIndex = 35
        Me.Label9.Text = "Top:"
        '
        'txtWidgetWidth
        '
        Me.txtWidgetWidth.Location = New System.Drawing.Point(44, 106)
        Me.txtWidgetWidth.Name = "txtWidgetWidth"
        Me.txtWidgetWidth.Size = New System.Drawing.Size(105, 20)
        Me.txtWidgetWidth.TabIndex = 38
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(6, 109)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(38, 13)
        Me.Label11.TabIndex = 37
        Me.Label11.Text = "Width:"
        '
        'txtViewName
        '
        Me.txtViewName.Location = New System.Drawing.Point(12, 19)
        Me.txtViewName.Name = "txtViewName"
        Me.txtViewName.Size = New System.Drawing.Size(610, 13)
        Me.txtViewName.TabIndex = 25
        Me.txtViewName.Text = "View Name"
        Me.txtViewName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pbView
        '
        Me.pbView.Location = New System.Drawing.Point(9, 43)
        Me.pbView.Name = "pbView"
        Me.pbView.Size = New System.Drawing.Size(613, 340)
        Me.pbView.TabIndex = 0
        Me.pbView.TabStop = False
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.SystemColors.ControlDark
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LelToolStripMenuItem, Me.toolstripitem, Me.HelpToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1316, 24)
        Me.MenuStrip1.TabIndex = 27
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'LelToolStripMenuItem
        '
        Me.LelToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mbtnProjNew, Me.mbtnProjOpen, Me.mbtnProjSave, Me.mbtnProjClose, Me.mbtnProjExit})
        Me.LelToolStripMenuItem.Name = "LelToolStripMenuItem"
        Me.LelToolStripMenuItem.Size = New System.Drawing.Size(56, 20)
        Me.LelToolStripMenuItem.Text = "Project"
        '
        'mbtnProjNew
        '
        Me.mbtnProjNew.Name = "mbtnProjNew"
        Me.mbtnProjNew.Size = New System.Drawing.Size(152, 22)
        Me.mbtnProjNew.Text = "New"
        '
        'mbtnProjOpen
        '
        Me.mbtnProjOpen.Name = "mbtnProjOpen"
        Me.mbtnProjOpen.Size = New System.Drawing.Size(152, 22)
        Me.mbtnProjOpen.Text = "Open"
        '
        'mbtnProjSave
        '
        Me.mbtnProjSave.Name = "mbtnProjSave"
        Me.mbtnProjSave.Size = New System.Drawing.Size(152, 22)
        Me.mbtnProjSave.Text = "Save"
        '
        'mbtnProjClose
        '
        Me.mbtnProjClose.Name = "mbtnProjClose"
        Me.mbtnProjClose.Size = New System.Drawing.Size(152, 22)
        Me.mbtnProjClose.Text = "Close"
        '
        'mbtnProjExit
        '
        Me.mbtnProjExit.Name = "mbtnProjExit"
        Me.mbtnProjExit.Size = New System.Drawing.Size(152, 22)
        Me.mbtnProjExit.Text = "Exit"
        '
        'toolstripitem
        '
        Me.toolstripitem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mbtnViewNew, Me.mbtnViewOpen, Me.mbtnViewSave, Me.mbtnViewClose})
        Me.toolstripitem.Name = "toolstripitem"
        Me.toolstripitem.Size = New System.Drawing.Size(44, 20)
        Me.toolstripitem.Text = "View"
        '
        'mbtnViewNew
        '
        Me.mbtnViewNew.Name = "mbtnViewNew"
        Me.mbtnViewNew.Size = New System.Drawing.Size(152, 22)
        Me.mbtnViewNew.Text = "New"
        '
        'mbtnViewOpen
        '
        Me.mbtnViewOpen.Name = "mbtnViewOpen"
        Me.mbtnViewOpen.Size = New System.Drawing.Size(152, 22)
        Me.mbtnViewOpen.Text = "Open"
        '
        'mbtnViewSave
        '
        Me.mbtnViewSave.Name = "mbtnViewSave"
        Me.mbtnViewSave.Size = New System.Drawing.Size(152, 22)
        Me.mbtnViewSave.Text = "Save"
        '
        'mbtnViewClose
        '
        Me.mbtnViewClose.Name = "mbtnViewClose"
        Me.mbtnViewClose.Size = New System.Drawing.Size(152, 22)
        Me.mbtnViewClose.Text = "Close"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mbtnHelpAbout})
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.HelpToolStripMenuItem.Text = "Help"
        '
        'mbtnHelpAbout
        '
        Me.mbtnHelpAbout.Name = "mbtnHelpAbout"
        Me.mbtnHelpAbout.Size = New System.Drawing.Size(206, 22)
        Me.mbtnHelpAbout.Text = "About FreeView Designer"
        '
        'FreeViewDesigner
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(1316, 696)
        Me.Controls.Add(Me.gbView)
        Me.Controls.Add(Me.gbProject)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "FreeViewDesigner"
        Me.Text = "FreeView Designer"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.gbProject.ResumeLayout(False)
        Me.gbProject.PerformLayout()
        CType(Me.pbProject, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbView.ResumeLayout(False)
        Me.gbView.PerformLayout()
        Me.gbWidget.ResumeLayout(False)
        Me.gbWidget.PerformLayout()
        CType(Me.pbWidget, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnWidgetRemove As System.Windows.Forms.Button
    Friend WithEvents lbWidgets As System.Windows.Forms.ListBox
    Friend WithEvents gbProject As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtProjFirstView As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents pbProject As System.Windows.Forms.PictureBox
    Friend WithEvents gbView As System.Windows.Forms.GroupBox
    Friend WithEvents pbView As System.Windows.Forms.PictureBox
    Friend WithEvents txtViewName As System.Windows.Forms.Label
    Friend WithEvents cbWidgetType As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtWidgetLeft As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtWidgetWidth As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtWidgetHeight As System.Windows.Forms.TextBox
    Friend WithEvents txtWidgetTop As System.Windows.Forms.TextBox
    Friend WithEvents txtWidgetName As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents gbWidget As System.Windows.Forms.GroupBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtWidgetTargetView As System.Windows.Forms.TextBox
    Friend WithEvents pbWidget As System.Windows.Forms.PictureBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtWidgetViewOffset As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtViewOffset As System.Windows.Forms.TextBox
    Friend WithEvents lblViewHeightRatio As System.Windows.Forms.Label
    Friend WithEvents lblViewWidthRatio As System.Windows.Forms.Label
    Friend WithEvents lblViewHeight As System.Windows.Forms.Label
    Friend WithEvents lblViewWidth As System.Windows.Forms.Label
    Friend WithEvents lblViewLength As System.Windows.Forms.Label
    Friend WithEvents lblViewPanoramaHeight As System.Windows.Forms.Label
    Friend WithEvents lblViewPanoramaLength As System.Windows.Forms.Label
    Friend WithEvents txtProjLoadOffset As System.Windows.Forms.TextBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents LelToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mbtnProjNew As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mbtnProjOpen As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mbtnProjExit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents toolstripitem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mbtnViewNew As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mbtnViewOpen As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mbtnProjSave As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mbtnViewSave As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnProjFirstView As System.Windows.Forms.Button
    Friend WithEvents btnWidgetTargetView As System.Windows.Forms.Button
    Friend WithEvents HelpToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mbtnHelpAbout As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents btnViewDrawWidget As System.Windows.Forms.Button
    Friend WithEvents cbViewWidgetType As System.Windows.Forms.ComboBox
    Friend WithEvents btnWidgetSetOffset As System.Windows.Forms.Button
    Friend WithEvents btnWidgetCancelSelection As System.Windows.Forms.Button
    Friend WithEvents txtProjPath As System.Windows.Forms.TextBox
    Friend WithEvents txtProjName As System.Windows.Forms.TextBox
    Friend WithEvents btnProjSetOffset As System.Windows.Forms.Button
    Friend WithEvents lbProjViews As System.Windows.Forms.ListBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtViewCropSize As System.Windows.Forms.TextBox
    Friend WithEvents mbtnViewClose As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mbtnProjClose As System.Windows.Forms.ToolStripMenuItem

End Class
