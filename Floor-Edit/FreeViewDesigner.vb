﻿Imports FreeView
Imports System.Drawing.Imaging
Imports System.IO


' FreeView Designer is a class/tool for creating location and view files for FreeView.
' The design of this app is split in three areas: Location, View and Actionwidget,  
' where in each area you can manipulate the relevant staple FreeView object.
Public Class FreeViewDesigner
    Public Project As Location = Nothing
    Public ProjectPath As String = Nothing
    Private CurrentView As View = Nothing

    ' PanoramaBoxes that replace PictureBoxes at the app's load
    Private PBoxProject As PanoramaBox
    Private PBoxView As PanoramaBox
    Private PBoxWidget As PanoramaBox

    ' ------ Drawing Variables ------
    ' Set True when the "Draw Widget" button has been clicked
    Private DrawMode As Boolean = False
    ' When drawing, WidgetDraft is displayed and being manipulated as a PictureBox 
    ' (drawing this way it's faster than if using an ActionWidget).
    ' After drawing is finished, an actual ActionWidget replaces the WidgetDraft and is added to the view
    Private WidgetDraft As PictureBox = Nothing
    ' Set True when user started drawing widget
    Public DrawingFlag As Boolean = False
    ' The point where user started drawing widget
    Private DrawAnchor As Point

    ' The frame that appears around a widget when the widget is highlighted.
    ' Under the hood it is just another widget drawn on the top of the target widget
    Private HighlightFrame As PanoramaBoxWidget
    Private HighlightedWidget As ActionWidget = Nothing

    Private WidgetNameChangedFlag As Boolean = False
    Private WidgetPosChangedFlag As Boolean = False
    Private LocCropSizeChangedFlag As Boolean = False

    Private Sub FloorEdit_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        SetupPanoramaBoxes()
        HighlightFrame = New PanoramaBoxWidget(My.Resources.HighlightFrame2, 0, 0, 0, 0, PBoxView)

        DisableAll()

        cbViewWidgetType.SelectedIndex = 0
    End Sub

    ' Replaces PictureBoxes from the [Design] by PanoramaBoxes,
    ' called only once in the Load function
    Private Sub SetupPanoramaBoxes()
        PBoxProject = New PanoramaBox()
        PBoxProject.Top = pbProject.Top
        PBoxProject.Left = pbProject.Left
        PBoxProject.Width = pbProject.Width
        PBoxProject.Height = pbProject.Height
        PBoxProject.Sample = My.Resources.noimage
        PBoxProject.CropSize = 1
        gbProject.Controls.Remove(pbProject)
        gbProject.Controls.Add(PBoxProject)

        PBoxView = New PanoramaBox()
        PBoxView.Top = pbView.Top
        PBoxView.Left = pbView.Left
        PBoxView.Width = pbView.Width
        PBoxView.Height = pbView.Height
        PBoxView.Sample = My.Resources.noimage
        PBoxView.CropSize = 1
        gbView.Controls.Remove(pbView)
        gbView.Controls.Add(PBoxView)
        AddHandler PBoxView.OffsetChanged, AddressOf PBoxViewOffsetChanged
        AddHandler PBoxView.MouseDown, AddressOf PBoxLocationMouseDown
        AddHandler PBoxView.MouseMove, AddressOf PBoxLocationMouseMove
        AddHandler PBoxView.MouseUp, AddressOf PBoxLocationMouseUp
        AddHandler PBoxView.WidgetClicked, AddressOf PBoxLocationWidgetClicked

        PBoxWidget = New PanoramaBox()
        PBoxWidget.Top = pbWidget.Top
        PBoxWidget.Left = pbWidget.Left
        PBoxWidget.Width = pbWidget.Width
        PBoxWidget.Height = pbWidget.Height
        PBoxWidget.Sample = My.Resources.noimage
        PBoxWidget.CropSize = 1
        gbWidget.Controls.Remove(pbWidget)
        gbWidget.Controls.Add(PBoxWidget)
    End Sub

    ' Disables all three areas
    Private Sub DisableAll()
        gbProject.Enabled = False
        txtProjPath.Text = ""
        txtProjName.Text = ""
        txtProjFirstView.Text = ""
        txtProjLoadOffset.Text = ""
        PBoxProject.CropSize = 1
        PBoxProject.Sample = My.Resources.noimage

        mbtnProjSave.Enabled = False
        mbtnProjClose.Enabled = False

        mbtnViewNew.Enabled = False
        mbtnViewOpen.Enabled = False

        lbProjViews.Items.Clear()

        DisableViewBox()
    End Sub
    ' Enable the Project area (after a Location file was opened or a new created)
    Private Sub EnableProjBox()
        gbProject.Enabled = True
        mbtnProjSave.Enabled = True
        mbtnProjClose.Enabled = True
        mbtnViewNew.Enabled = True
        mbtnViewOpen.Enabled = True
    End Sub
    ' Enable the View area (after a View file was selected/opened or a new created)
    Private Sub EnableViewBox()
        gbView.Enabled = True
        mbtnViewSave.Enabled = True
        mbtnViewClose.Enabled = True

        UpdateViewData()
        UpdateWidgetList()
    End Sub
    ' Enable Select/Open View (after a Location file was opened or a new created)
    Private Sub EnableSelectOpenView()
        mbtnViewNew.Enabled = True
        mbtnViewOpen.Enabled = True
    End Sub

    Private Sub DisableViewBox()
        PBoxView.CropSize = 1
        PBoxView.Sample = My.Resources.noimage
        HighlightFrame.SetPos(0, 0, 0, 0)
        PBoxView.AddWidget(HighlightFrame)

        mbtnViewSave.Enabled = False
        mbtnViewClose.Enabled = False

        txtViewOffset.Text = ""
        txtViewCropSize.Text = ""
        lblViewWidth.Text = "Width:"
        lblViewHeight.Text = "Height:"
        lblViewLength.Text = "Length:"
        lblViewPanoramaHeight.Text = "Panorama Height:"
        lblViewPanoramaLength.Text = "Panorama Length:"
        lblViewHeightRatio.Text = "Height Ratio:"
        lblViewWidthRatio.Text = "Width Ratio:"

        gbView.Enabled = False

        DisableWidgetBox()
    End Sub

    Private Sub UpdateViewData()
        lblViewWidth.Text = "Width: " & PBoxView.Width
        lblViewHeight.Text = "Height: " & PBoxView.Height
        lblViewLength.Text = "Length: " & PBoxView.Length
        lblViewPanoramaHeight.Text = "Panorama Height: " & PBoxView.Sample.Height
        lblViewPanoramaLength.Text = "Panorama Length: " & PBoxView.Sample.Width
        lblViewWidthRatio.Text = "Width Ratio: " & FormatNumber(PBoxView.WidthRatio, 2)
        lblViewHeightRatio.Text = "Height Ratio: " & FormatNumber(PBoxView.HeightRatio, 2)
        txtViewName.Text = CurrentView.Name
        txtViewCropSize.Text = PBoxView.CropSize
    End Sub
    Private Sub UpdateHighlightedWidgetData()
        If HighlightedWidget Is Nothing Then
            Return
        End If

        If HighlightedWidget.Name = "" Then
            txtWidgetName.Text = "<unnamed>"
        Else
            txtWidgetName.Text = HighlightedWidget.Name
        End If

        If HighlightedWidget.Type = ActionWidget.WidgetType.Passage Then
            cbWidgetType.SelectedIndex = 0
        ElseIf HighlightedWidget.Type = ActionWidget.WidgetType.Power Then
            cbWidgetType.SelectedIndex = 1
        ElseIf HighlightedWidget.Type = ActionWidget.WidgetType.Zoom Then
            cbWidgetType.SelectedIndex = 2
        ElseIf HighlightedWidget.Type = ActionWidget.WidgetType.Retrn Then
            cbWidgetType.SelectedIndex = 3
        End If

        txtWidgetLeft.Text = HighlightedWidget.Left
        txtWidgetTop.Text = HighlightedWidget.Top
        txtWidgetWidth.Text = HighlightedWidget.Width
        txtWidgetHeight.Text = HighlightedWidget.Height

        txtWidgetTargetView.Text = HighlightedWidget.TargetView
        If HighlightedWidget.TargetView <> "" Then
            Dim loc As View = New View(ViewNameToFilename(HighlightedWidget.TargetView))
            loc.Load()
            txtWidgetViewOffset.Text = FormatNumber(HighlightedWidget.TargetViewLoadOffset, 2)

            PBoxWidget.Sample = loc.Panorama
            PBoxWidget.CropSize = loc.PanoramaCropSize
            PBoxWidget.Offset = HighlightedWidget.TargetViewLoadOffset * PBoxWidget.Length
        Else
            PBoxWidget.Sample = My.Resources.noimage
            PBoxWidget.Offset = 0
            txtWidgetViewOffset.Text = ""
        End If
    End Sub
    Private Sub UpdateWidgetList()
        Dim selectedIndex As Integer = lbWidgets.SelectedIndex
        lbWidgets.Items.Clear()

        Dim index As Integer = 0
        While index < PBoxView.GetCount() - 1 ' Last widget is HighlightFrame
            Dim widget As ActionWidget = PBoxView.GetWidget(index)
            If widget.Name = "" Then
                lbWidgets.Items.Add("<unnamed>")
            Else
                lbWidgets.Items.Add(widget.Name)
            End If

            index += 1
        End While

        If selectedIndex <> -1 Then
            lbWidgets.SelectedIndex = selectedIndex
        End If
    End Sub

    Private Sub HighlightWidget(widget As ActionWidget)
        HighlightFrame.SetPos(widget.Top, widget.Left, widget.Height, widget.Width)
        HighlightedWidget = widget
        UpdateHighlightedWidgetData()
        EnableWidgetBox()
    End Sub

    ' WidgetBox is enabled after highlighting a widget and after deselection
    ' of the widget it becomes disabled again
    Private Sub DisableWidgetBox()
        CancelWidgetSelection()
        gbWidget.Enabled = False

        txtWidgetName.Text = ""

        cbWidgetType.SelectedIndex = 0

        txtWidgetLeft.Text = ""
        txtWidgetTop.Text = ""
        txtWidgetWidth.Text = ""
        txtWidgetHeight.Text = ""

        txtWidgetTargetView.Text = ""
        txtWidgetViewOffset.Text = ""

        PBoxWidget.Sample = Nothing
    End Sub
    ' Enable Widget Area and fill it with data
    Private Sub EnableWidgetBox()
        gbWidget.Enabled = True
    End Sub


    Private Sub EnableDrawMode()
        DrawMode = True
        btnViewDrawWidget.Text = "Cancel Drawing"
        cbViewWidgetType.Enabled = False
        PBoxView.Hold()
    End Sub
    Private Sub DisableDrawMode()
        DrawMode = False
        btnViewDrawWidget.Text = "Draw Widget"
        cbViewWidgetType.Enabled = True
        PBoxView.Release()

        If WidgetDraft IsNot Nothing Then
            PBoxView.Controls.Remove(WidgetDraft)
        End If

        WidgetDraft = Nothing
    End Sub

    ' If in DrawMode, init drawing
    Private Sub PBoxLocationMouseDown(sender As Object, e As MouseEventArgs)
        If DrawMode = True Then
            WidgetDraft = New PictureBox()
            WidgetDraft.SizeMode = PictureBoxSizeMode.StretchImage
            WidgetDraft.BackColor = Color.Transparent

            If cbViewWidgetType.Text = "PassageWidget" Then
                WidgetDraft.Image = My.Resources.PassageWidgetEnter
            ElseIf cbViewWidgetType.Text = "ZoomWidget" Then
                WidgetDraft.Image = My.Resources.ZoomWidgetEnter
            ElseIf cbViewWidgetType.Text = "PowerWidget" Then
                WidgetDraft.Image = My.Resources.PowerWidgetEnter
            ElseIf cbViewWidgetType.Text = "ReturnWidget" Then
                WidgetDraft.Image = My.Resources.ReturnWidgetEnter
            End If

            PBoxView.Controls.Add(WidgetDraft)
            DrawAnchor = New Point(e.X, e.Y)
            WidgetDraft.Left = e.X
            WidgetDraft.Top = e.Y

            DrawingFlag = True
        End If
    End Sub
    ' If in DrawMode, resize the WidgetDraft as mouse moves
    Private Sub PBoxLocationMouseMove(sender As Object, e As MouseEventArgs)
        If DrawingFlag = False Then
            Return
        End If

        If e.X < DrawAnchor.X Then
            WidgetDraft.Left = e.X
            WidgetDraft.Width = DrawAnchor.X - e.X
        Else
            WidgetDraft.Left = DrawAnchor.X
            WidgetDraft.Width = e.X - DrawAnchor.X
        End If

        If e.Y < DrawAnchor.Y Then
            WidgetDraft.Top = e.Y
            WidgetDraft.Height = DrawAnchor.Y - e.Y
        Else
            WidgetDraft.Top = DrawAnchor.Y
            WidgetDraft.Height = e.Y - DrawAnchor.Y
        End If
    End Sub
    ' If in DrawMode, complete drawing and replace the WidgetDraft by an actual Widget
    Private Sub PBoxLocationMouseUp(sender As Object, e As MouseEventArgs)
        If DrawingFlag = False Then
            Return
        End If

        ' Remove HighlightFrame
        PBoxView.RemoveWidget(PBoxView.GetCount() - 1)

        DrawingFlag = False
        If cbViewWidgetType.Text = "PassageWidget" Then
            Dim pw As New PassageWidget((WidgetDraft.Left + PBoxView.Offset) Mod PBoxView.Length, WidgetDraft.Top, WidgetDraft.Width, WidgetDraft.Height, PBoxView)
        ElseIf cbViewWidgetType.Text = "ZoomWidget" Then
            Dim pw As New ZoomWidget((WidgetDraft.Left + PBoxView.Offset) Mod PBoxView.Length, WidgetDraft.Top, WidgetDraft.Width, WidgetDraft.Height, PBoxView)
        ElseIf cbViewWidgetType.Text = "PowerWidget" Then
            Dim pw As New PowerWidget((WidgetDraft.Left + PBoxView.Offset) Mod PBoxView.Length, WidgetDraft.Top, WidgetDraft.Width, WidgetDraft.Height, PBoxView)
        ElseIf cbViewWidgetType.Text = "ReturnWidget" Then
            Dim pw As New ReturnWidget((WidgetDraft.Left + PBoxView.Offset) Mod PBoxView.Length, WidgetDraft.Top, WidgetDraft.Width, WidgetDraft.Height, PBoxView)
        End If

        ' Bring back HighlightFrame
        PBoxView.AddWidget(HighlightFrame)

        DisableDrawMode()
        UpdateWidgetList()
        lbWidgets.SelectedIndex = PBoxView.GetCount() - 2

        EnableWidgetBox()
    End Sub
    ' Highlight clicked widget
    Private Sub PBoxLocationWidgetClicked(widget As PanoramaBoxWidget)
        HighlightWidget(widget)
    End Sub
    Private Sub PBoxViewOffsetChanged()
        txtViewOffset.Text = PBoxView.Offset
    End Sub
    ' Highlight widget which's index has been selected
    Private Sub cbWidgetList_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lbWidgets.SelectedIndexChanged
        If lbWidgets.SelectedIndex = -1 Then
            DisableWidgetBox()
            Return
        End If

        HighlightWidget(PBoxView.GetWidget(lbWidgets.SelectedIndex))
    End Sub
    ' Load selected View into the ViewBox
    Private Sub lbView_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lbProjViews.SelectedIndexChanged
        If lbProjViews.SelectedIndex = -1 Then
            Return
        End If

        Dim view As New View(ViewNameToFilename(lbProjViews.Items(lbProjViews.SelectedIndex)))
        CurrentView = view
        SetupCurrentView()
    End Sub

    Private Sub mbtnProjNew_Click(sender As Object, e As EventArgs) Handles mbtnProjNew.Click
        Dim fbd As New FolderBrowserDialog()
        fbd.Description = "Select or create a new folder for your project."

        If fbd.ShowDialog() <> Windows.Forms.DialogResult.OK Then
            Return
        End If

        Dim projectName As String = InputBox("Enter name for new project")
        If projectName = "" Then
            Return
        End If

        ProjectPath = fbd.SelectedPath
        Project = New Location(ProjectPath & "\" & projectName & "." & FreeView.Location.Extension)

        DisableAll()
        DisableViewBox()
        UpdateProjectData()
    End Sub
    Private Sub mbtnProjSave_Click(sender As Object, e As EventArgs) Handles mbtnProjSave.Click
        Project.Save()
        MsgBox("Project saved")
    End Sub
    Private Sub mbtnProjOpen_Click(sender As Object, e As EventArgs) Handles mbtnProjOpen.Click
        Dim ofd As New OpenFileDialog()
        ofd.Title = "Open project"
        ofd.Filter = "FreeView Location Files (*.loc) | *.loc"

        If ofd.ShowDialog() <> Windows.Forms.DialogResult.OK Then
            Return
        End If

        Try
            Project = New Location(ofd.FileName)
            Project.Load()

            DisableAll()
            UpdateProjectData()
        Catch ex As Exception
            MsgBox("The location file seems to be corrupted and cannot be open.", MsgBoxStyle.OkOnly, "Error")
            Return
        End Try
    End Sub

    ' Change first View loaded in the project
    Private Sub btnProjFirstView_Click(sender As Object, e As EventArgs) Handles btnProjFirstView.Click
        Dim firstView As View = OpenView()
        If firstView Is Nothing Then
            Return
        End If

        Project.FirstViewName = firstView.Name
        Project.ViewLoadOffset = 0

        txtProjFirstView.Text = Project.FirstViewName
        txtProjLoadOffset.Text = Project.ViewLoadOffset
        PBoxProject.Sample = firstView.Panorama
        PBoxProject.CropSize = firstView.PanoramaCropSize
    End Sub
    ' Changee first View's load offset
    Private Sub btnProjSetOffset_Click(sender As Object, e As EventArgs) Handles btnProjSetOffset.Click
        Project.ViewLoadOffset = PBoxProject.Offset / PBoxProject.Length
        txtProjLoadOffset.Text = FormatNumber(Project.ViewLoadOffset, 2)
    End Sub
    Private Sub UpdateProjectData()
        ProjectPath = Path.GetDirectoryName(Project.Filename)
        txtProjPath.Text = ProjectPath
        txtProjName.Text = Project.Name
        EnableProjBox()
        EnableSelectOpenView()
        mbtnProjSave.Enabled = True

        txtProjFirstView.Text = Project.FirstViewName
        txtProjLoadOffset.Text = FormatNumber(Project.ViewLoadOffset, 2)

        If Project.FirstViewName <> "" Then
            Dim firstLoc As New View(ViewNameToFilename(Project.FirstViewName))

            PBoxProject.Sample = firstLoc.Panorama
            PBoxProject.CropSize = firstLoc.PanoramaCropSize
            PBoxProject.Offset = PBoxProject.Length * Project.ViewLoadOffset
        End If

        UpdateViewsList()
    End Sub
    Private Sub UpdateViewsList()
        lbProjViews.Items.Clear()

        For Each file As String In My.Computer.FileSystem.GetFiles(ProjectPath)
            If Path.GetExtension(file) = "." & FreeView.View.Extension Then
                lbProjViews.Items.Add(Path.GetFileNameWithoutExtension(file))
            End If
        Next
    End Sub

    Private Sub mbtnViewNew_Click(sender As Object, e As EventArgs) Handles mbtnViewNew.Click
        Dim ofd As New OpenFileDialog()
        ofd.Title = "Select panorama picture for new location"
        ofd.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png, *.bmp, *.gif) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png; *.bmp; *.gif | All files (*.*) | *.*"

        If ofd.ShowDialog() <> Windows.Forms.DialogResult.OK Then
            Return
        End If

        Dim viewName As String = InputBox("Enter name for new location")
        If viewName = "" Then
            Return
        End If

        CurrentView = New View(ViewNameToFilename(viewName))

        PBoxView.CropSize = 0.2
        PBoxView.Load(ofd.FileName)

        PBoxView.AddWidget(HighlightFrame)
        EnableViewBox()
    End Sub
    Private Sub mbtnLocOpen_Click(sender As Object, e As EventArgs) Handles mbtnViewOpen.Click
        Dim view As View = OpenView()
        If view Is Nothing Then
            Return
        End If

        CurrentView = view
        SetupCurrentView()
    End Sub
    Private Sub mbtnLocSave_Click(sender As Object, e As EventArgs) Handles mbtnViewSave.Click
        'Dim loc As Location = GetUnsavedLocation(txtLocationName.Text)
        Dim view As View = CurrentView

        PBoxView.RemoveWidget(PBoxView.GetCount() - 1)

        view.LoadDataFromPanoramaBox(PBoxView)
        view.Save()

        PBoxView.AddWidget(HighlightFrame)

        MsgBox("View saved.")
    End Sub
    Private Sub btnViewDrawWidget_Click(sender As Object, e As EventArgs) Handles btnViewDrawWidget.Click
        If DrawMode = False Then
            EnableDrawMode()
        Else ' DrawMode = True
            DisableDrawMode()
        End If
    End Sub

    ' Open View
    Private Function OpenView() As View
        Dim ofd As New OpenFileDialog()
        ofd.Title = "Open location"
        ofd.Filter = "FreeView View Files (*.view) | *.view"
        ofd.InitialDirectory = ProjectPath

        Do
            If ofd.ShowDialog() = Windows.Forms.DialogResult.OK Then
                If Path.GetDirectoryName(ofd.FileName) <> ProjectPath Then
                    If MsgBox("You have to select location from your project's directory." & vbNewLine _
                           & "It is default directory when you access open file dialog. Try again?", MsgBoxStyle.OkCancel) = MsgBoxResult.Cancel Then
                        Return Nothing
                    End If
                Else
                    ' Success
                    Exit Do
                End If
            Else
                Return Nothing
            End If

            ofd.FileName = ""
            ofd.InitialDirectory = ProjectPath
        Loop

        Dim view As View
        Try
            view = New View(ofd.FileName)
        Catch ex As Exception
            view = Nothing
            MsgBox("There was a problem with opening new view.", "Sorry")
        End Try

        Return view
    End Function
    ' Given a name only (in .loc and .view files only names are saved so they are not tied to any directories or paths) 
    ' merge it with the project's path and create a filename that can be then loaded to the program) 
    Private Function ViewNameToFilename(name As String) As String
        Return ProjectPath & "\" & name & "." & FreeView.View.Extension
    End Function

    ' After selecting different View put its data onto the ViewBox
    Private Sub SetupCurrentView()
        CurrentView.Load()

        PBoxView.Sample = CurrentView.Panorama
        PBoxView.CropSize = CurrentView.PanoramaCropSize

        For Each widget As ActionWidget In CurrentView.Widgets
            PBoxView.AddWidget(widget)
        Next
        PBoxView.AddWidget(HighlightFrame)

        DisableWidgetBox()
        UpdateWidgetList()
        EnableViewBox()
    End Sub

    Private Sub btnWidgetCancelSelection_Click(sender As Object, e As EventArgs) Handles btnWidgetCancelSelection.Click
        DisableWidgetBox()
    End Sub

    ' Cancel highlight of the widget, if any highlighted
    Private Sub CancelWidgetSelection()
        If lbWidgets.SelectedIndex <> -1 Then
            lbWidgets.SelectedIndex = -1
        End If

        If HighlightedWidget Is Nothing Then
            Return
        End If

        HighlightFrame.SetPos(0, 0, 0, 0)
    End Sub

    ' Set target view for higlighted widget
    Private Sub btnWidgetTargetView_Click(sender As Object, e As EventArgs) Handles btnWidgetTargetView.Click
        Dim view As View = OpenView()
        If view Is Nothing Then
            Return
        End If

        HighlightedWidget.TargetView = view.Name
        HighlightedWidget.TargetViewLoadOffset = 0

        UpdateHighlightedWidgetData()
    End Sub
    ' Set target view offset for higlighted widget
    Private Sub btnWidgetSetOffset_Click(sender As Object, e As EventArgs) Handles btnWidgetSetOffset.Click
        HighlightedWidget.TargetViewLoadOffset = PBoxWidget.Offset / PBoxWidget.Length
        UpdateHighlightedWidgetData()
    End Sub
    ' Remove higlighted widget from the view
    Private Sub btnWidgetRemove_Click(sender As Object, e As EventArgs) Handles btnWidgetRemove.Click
        If HighlightedWidget IsNot Nothing Then
            Dim wdg As PanoramaBoxWidget = HighlightedWidget
            DisableWidgetBox()
            PBoxView.RemoveWidget(wdg)
            UpdateWidgetList()
        End If
    End Sub

    ' ---- Handle name change of higlighted widget ----
    Private Sub txtWidgetName_TextChanged(sender As Object, e As EventArgs) Handles txtWidgetName.TextChanged
        WidgetNameChangedFlag = True
    End Sub
    Private Sub txtWidgetName_KeyDown(sender As Object, e As KeyEventArgs) Handles txtWidgetName.KeyDown
        If e.KeyCode = Keys.Enter Then
            WidgetNameSubmit()
            e.SuppressKeyPress = True
        End If
    End Sub
    Private Sub txtWidgetName_Leave(sender As Object, e As EventArgs) Handles txtWidgetName.Leave
        WidgetNameSubmit()
    End Sub
    Private Sub WidgetNameSubmit()
        If WidgetNameChangedFlag = False Then
            Return
        End If

        WidgetNameChangedFlag = False
        HighlightedWidget.Name = txtWidgetName.Text
        UpdateWidgetList()
    End Sub

    ' ---- Handle position change of higlighted widget ----
    Private Sub WidgetPosChanged(sender As Object, e As EventArgs) Handles txtWidgetLeft.TextChanged, txtWidgetTop.TextChanged, txtWidgetWidth.TextChanged, txtWidgetHeight.TextChanged
        WidgetPosChangedFlag = True
    End Sub
    Private Sub WidgetPosKeyDown(sender As Object, e As KeyEventArgs) Handles txtWidgetLeft.KeyDown, txtWidgetTop.KeyDown, txtWidgetWidth.KeyDown, txtWidgetHeight.KeyDown
        If e.KeyCode = Keys.Enter Then
            WidgetPosSubmit()
            e.SuppressKeyPress = True
        End If
    End Sub
    Private Sub WidgetPosLeave(sender As Object, e As EventArgs) Handles txtWidgetLeft.Leave, txtWidgetTop.Leave, txtWidgetWidth.Leave, txtWidgetHeight.Leave
        WidgetPosSubmit()
    End Sub
    Private Sub WidgetPosSubmit()
        If WidgetPosChangedFlag = False Then
            Return
        End If

        WidgetPosChangedFlag = False
        Dim validFlag As Boolean = True
        Dim top As Integer, left As Integer, height As Integer, width As Integer

        Try
            top = txtWidgetTop.Text
            left = txtWidgetLeft.Text
            height = txtWidgetHeight.Text
            width = txtWidgetWidth.Text
        Catch ex As Exception
            validFlag = False
        End Try

        If validFlag <> False Then
            If top < 0 Or left < 0 Or height <= 0 Or width <= 0 Then
                validFlag = False
            ElseIf top >= PBoxView.Height Or height > PBoxView.Height Or left >= PBoxView.Length Or width > PBoxView.Length Then
                validFlag = False
            End If
        End If

        If validFlag = False Then
            txtWidgetTop.Text = HighlightedWidget.Top
            txtWidgetLeft.Text = HighlightedWidget.Left
            txtWidgetWidth.Text = HighlightedWidget.Width
            txtWidgetHeight.Text = HighlightedWidget.Height

            MsgBox("Widget position and dimension values must be positive numbers within panorama length and height range.", MsgBoxStyle.OkOnly, "Error")
        Else
            HighlightedWidget.SetPos(txtWidgetTop.Text, txtWidgetLeft.Text, txtWidgetHeight.Text, txtWidgetWidth.Text)
            HighlightWidget(HighlightedWidget)
        End If
    End Sub

    ' ---- Handle crop size change of current location ----
    Private Sub txtLocCropSize_TextChanged(sender As Object, e As EventArgs) Handles txtViewCropSize.TextChanged
        LocCropSizeChangedFlag = True
    End Sub
    Private Sub txtLocCropSize_KeyDown(sender As Object, e As KeyEventArgs) Handles txtViewCropSize.KeyDown
        If e.KeyCode = Keys.Enter Then
            LocCropSizeSubmit()
            e.SuppressKeyPress = True
        End If
    End Sub
    Private Sub txtLocCropSize_Leave(sender As Object, e As EventArgs) Handles txtViewCropSize.Leave
        LocCropSizeSubmit()
    End Sub
    Private Sub LocCropSizeSubmit()
        If LocCropSizeChangedFlag = False Then
            Return
        End If

        LocCropSizeChangedFlag = True
        Dim validFlag As Boolean = True
        If IsNumeric(txtViewCropSize.Text) = False Then
            validFlag = False
        Else
            Dim val As Double
            Try
                val = txtViewCropSize.Text
            Catch ex As Exception
                val = 0
            End Try

            If val <= 0 Or val > 1 Then
                validFlag = False
            End If

        End If

        If validFlag = True Then
            PBoxView.CropSize = txtViewCropSize.Text
        Else
            MsgBox("Crop Size must be a numeric value between 0 and 1.", MsgBoxStyle.OkOnly, "Error")
            txtViewCropSize.Text = PBoxView.CropSize
        End If
    End Sub

    Private Sub mbtnHelpAbout_Click(sender As Object, e As EventArgs) Handles mbtnHelpAbout.Click
        frmAbout.Show()
    End Sub

    Private Sub mbtnProjClose_Click(sender As Object, e As EventArgs) Handles mbtnProjClose.Click
        DisableAll()
    End Sub

    Private Sub mbtnViewClose_Click(sender As Object, e As EventArgs) Handles mbtnViewClose.Click
        DisableViewBox()
    End Sub
End Class
