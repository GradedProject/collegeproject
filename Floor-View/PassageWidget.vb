﻿Imports System.Drawing.Imaging

Public Class PassageWidget
    Inherits ActionWidget

    Public Sub New(left As Integer, top As Integer, width As Integer, height As Integer, Optional pBox As PanoramaBox = Nothing)
        MyBase.New(My.Resources.PassageWidgetLeave, left, top, width, height, WidgetType.Passage, pBox)
        SampleFocus = My.Resources.PassageWidgetEnter
    End Sub

End Class
