﻿' ActionWidget class is a base class for widgets designed
' for FreeView application. ActionWidgets derive from the PanoramaBoxWidget class
' and have a couple additional properties on the top of their base class.
Public Class ActionWidget
    Inherits PanoramaBoxWidget
    Private mName As String = ""
    Private mTargetView As String = ""
    Private mTargetViewLoadOffset As Double = 0

    ' Type of a widget defines its shape and purpose.
    ' #Note: At the moment widget types only differ by their images.
    Public ReadOnly Type As WidgetType
    ' Name of the widget (optional)
    Public Property Name As String
        Get
            Return mName
        End Get
        Set(name As String)
            mName = name
            RaiseChanged()
        End Set
    End Property
    ' Name of location file that the widget leads to
    Public Property TargetView As String
        Get
            Return mTargetView
        End Get
        Set(name As String)
            mTargetView = name
            RaiseChanged()
        End Set
    End Property
    ' Offset value on which the widgets location should be loaded at
    Public Property TargetViewLoadOffset As Double
        Get
            Return mTargetViewLoadOffset
        End Get
        Set(offset As Double)
            mTargetViewLoadOffset = offset
        End Set
    End Property

    Sub New(sample As Bitmap, left As Integer, top As Integer, width As Integer, height As Integer, Optional type As WidgetType = Nothing, Optional pBox As PanoramaBox = Nothing)
        MyBase.New(sample, left, top, width, height, pBox)

        Me.Type = type
    End Sub

    Enum WidgetType
        Passage
        Zoom
        Power
        Retrn
    End Enum
End Class
