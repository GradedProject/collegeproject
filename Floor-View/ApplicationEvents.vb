﻿Imports System.IO

Namespace My

    ' The following events are available for MyApplication:
    ' 
    ' Startup: Raised when the application starts, before the startup form is created.
    ' Shutdown: Raised after all application forms are closed.  This event is not raised if the application terminates abnormally.
    ' UnhandledException: Raised if the application encounters an unhandled exception.
    ' StartupNextInstance: Raised when launching a single-instance application and the application is already active. 
    ' NetworkAvailabilityChanged: Raised when the network connection is connected or disconnected.
    Partial Friend Class MyApplication
        ''' <summary>This method catches all Unhandled Exceptions throw by the program and handles with them by creating a crash report, and telling the user that it'll quit.</summary>
        ''' <return>A crash log and a message.</return>
        ''' <history>[Adrian Iwaszkiewicz] 23/2/2015</history>
        Private Sub MyApplication_UnhandledException(ByVal sender As Object, ByVal e As Microsoft.VisualBasic.ApplicationServices.UnhandledExceptionEventArgs) Handles Me.UnhandledException
            ' Exception has been thrown here. Now need to print a crash report with relevant information about the program, and what possibly went wrong.
            ' A new file is being defined.
            Dim file As System.IO.StreamWriter
            ' Define where the file will be created and get it ready to edit it.
            file = My.Computer.FileSystem.OpenTextFileWriter(Path.GetDirectoryName(FreeView.CurrentLocation.Filename) & "\error.log", True)
            ' Write information to the file.
            file.WriteLine("Message----------")
            file.Write(e.Exception.Message)
            file.WriteLine("StackTrace-------")
            file.Write(e.Exception.StackTrace)
            file.WriteLine("InnerException---")
            file.Write(e.Exception.InnerException)
            ' Close the file, so it isn't in the inner (inside program) editor.
            file.Close()

            ' Show user that the application has been crashed and the crash report has been generated (inside a message box)
            MsgBox("A Crash has occured ! The program will exit now to prevent any damage ! Crash Report has been created !", MsgBoxStyle.OkOnly, "Oops, something broke...")
            ' Shut down the application, after the user clicked ok !
            'e.ExitApplication
        End Sub
    End Class


End Namespace

