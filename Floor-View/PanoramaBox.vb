﻿' Class designed to contain long/panoramic pictures that
' are meant be browsed instead of being whole (or only partially) displayed at once. 
'
' Use "Sample" property or "Load" function to load pictures
'
' Use ordinary Windows Forms classes to add controls designed for PictureBox
' Use "PanoramaBoxWidget" class' derives to add controls designed for PanoramaBox
' Note #1: Putting Widgets and/or Controls that overlap each other is not supported at the moment
'          and will cause undefined behavior.
'
' Note #2: Some images are not handled apropriately, especially images with height/length smaller than
'          the box's height/width. This issue is under investigation and will be fixed.
Public Class PanoramaBox
    Inherits PictureBox

    ' Raises when the panorama has been shifted(browsed)
    Public Event OffsetChanged()
    ' Raises
    Public Event WidgetClicked(widget As PanoramaBoxWidget)

    ' Sample holds original bitmap for the PanoramaBox with original dimensions
    Private mSample As Bitmap 
    Public Property Sample As Bitmap
        Get
            Return mSample
        End Get
        Set(bmp As Bitmap)
            ClearWidgets()
            Offset = 0

            If bmp IsNot Nothing Then
                mSample = bmp
                RebuildSampleFitted()
                RepaintBox()
            Else
                mSample = My.Resources.noimage
                CropSize = 1
            End If
        End Set
    End Property

    ' Sample bitmap resized to the box's current size
    ' Images cropped from this bitmap will fit the box's size, so there
    ' is no need to use PictureBox.Mode.StretchSize and it's faster.
    Private SampleFitted As Bitmap
    ' Instead of rebuilding SampleFitted every repaint, where most of the time it would be rebuilt the same,
    ' this function is created to be called everytime the box changes (i.e. size change) and the bitmap has to be rebuilt
    Private Sub RebuildSampleFitted()
        SampleFitted = New Bitmap(Sample, New Size(Length, Height))

        For Each widget As PanoramaBoxWidget In Widgets
            PaintWidget(widget)
        Next
    End Sub

    ' CropSize holds the fraction value of how big part of a picture should be displayed in the box
    Private mCropSize As Double = 1 / 5
    Public Property CropSize As Double
        Get
            Return mCropSize
        End Get
        Set(size As Double)
            Dim cropRatio As Double = CropSize / size
            mCropSize = size
            RebuildSampleFitted()
            Offset = Offset * cropRatio
            RepaintBox()
        End Set
    End Property

    ' Offset property holds the offset at which current part of a picture is displayed
    Private mOffset As Integer
    Public Property Offset As Integer
        Get
            Return mOffset * WidthRatio
        End Get
        Set(offset As Integer)
            mOffset = offset / WidthRatio
            RepaintBox()
            RaiseEvent OffsetChanged()
        End Set
    End Property

    ' The proportion between the Sample's width and the SampleFitted's height 
    Public ReadOnly Property WidthRatio As Double
        Get
            If Sample Is Nothing Then
                Return 1
            End If

            Return Me.Width / (Sample.Width * CropSize)
        End Get
    End Property
    ' The proportion between the Sample's height and the SampleFitted's height 
    Public ReadOnly Property HeightRatio As Double
        Get
            Return Me.Height / Sample.Height
        End Get
    End Property
    ' Returns the total length of the panorama, 
    ' unlike "Width" property which returns the width of the box
    Public ReadOnly Property Length() As Integer
        Get
            Return Sample.Width * WidthRatio
        End Get
    End Property

    Private MouseDownFlag As Boolean = False
    Private MouseDownPt As Point
    Private MouseEnterWidget As PanoramaBoxWidget = Nothing
    Private HoldFlag As Boolean = False

    Public Shadows Sub Load(filename As String)
        Sample = Bitmap.FromFile(filename)
    End Sub

    ' RepaintBox() function repaints SampleFitted on the PanoramaBox when the box's offset or size changes.
    ' In case of the size change, RebuildSampleFitted() function should be called prior to RepaintBox() function
    Public Sub RepaintBox()
        If Sample Is Nothing Then
            Return
        End If

        ' The offset is not far enough to roll back to the beginning of the panorama.
        ' Simply crop a picture from the panorama.
        If (Offset + Width <= Length) Then
            ' Code left for comparison how much slower it is when not using SampleFitted 
            'Dim rect As New Rectangle(Offset, 0, Sample.Width * CropSize, Sample.Height)
            'Dim bmp As Bitmap = Sample.Clone(rect, Sample.PixelFormat)
            'SizeMode = PictureBoxSizeMode.StretchImage

            Dim rect As New Rectangle(Offset, 0, Width, Height)
            Dim bmp As Bitmap = SampleFitted.Clone(rect, Sample.PixelFormat)

            If MyBase.Image IsNot Nothing Then
                MyBase.Image.Dispose()
            End If

            MyBase.Image = bmp

            ' Offset is far enough to roll back to the beginning of the panorama.
            ' Crop the part at the end (regular rectangle) and the part at the beginning (excess rectangle) and then merge them.
        Else ' Offset + Width > Length
            '*************************************
            '*          end  |  start            *
            '* regRect  of > | < of   excessRect *
            '*          pic  |   pic             * 
            '*************************************
            '/\             /\                  /\
            'Offset       Length           Offset+Width


            Dim bmp As Bitmap = New Bitmap(Width, Height)
            Dim excess As Integer = Offset + Width - Length

            Dim regRect As New Rectangle(Offset, 0, Width - excess, Height)
            Dim excessRect As New Rectangle(0, 0, excess, Height)

            Dim regBmp As Bitmap = SampleFitted.Clone(regRect, SampleFitted.PixelFormat)
            Dim excessBmp As Bitmap = SampleFitted.Clone(excessRect, SampleFitted.PixelFormat)

            Dim g As Graphics = Graphics.FromImage(MyBase.Image)
            g.DrawImage(regBmp, 0, 0, Width - excess, Height)
            g.DrawImage(excessBmp, Width - excess, 0, excess, Height)
            regBmp.Dispose()
            excessBmp.Dispose()

            Refresh()
        End If
    End Sub

    ' Handles MouseDown event.
    ' First checks if a widget has not been clicked,
    ' then if the PanroamaBox is not on hold state, initiates browsing of its picture
    Private Sub SubMouseDown(sender As Object, e As MouseEventArgs) Handles Me.MouseDown
        If HoldFlag = True Then
            Return
        End If

        Dim pbw As PanoramaBoxWidget = GetWidget((Offset + e.X) Mod Length, e.Y)

        If pbw IsNot Nothing Then
            RaiseEvent WidgetClicked(pbw)
        Else
            Me.Cursor = Cursors.NoMoveHoriz
            MouseDownPt = e.Location
            MouseDownFlag = True
        End If
    End Sub
    ' Shifts the picture when MouseDownFlag is true
    ' Also responsible for firing widgets MouseEnter and MouseLeave events 
    Private Sub SubMouseMove(sender As Object, e As MouseEventArgs) Handles Me.MouseMove
        If MouseDownFlag Then
            Shift((e.X - MouseDownPt.X) * 3)
            MouseDownPt.X = e.X
        Else
            Dim widget As PanoramaBoxWidget = GetWidget((e.X + Offset) Mod Length, e.Y)

            If widget IsNot Nothing Then
                If widget IsNot MouseEnterWidget Then
                    If MouseEnterWidget IsNot Nothing Then
                        MouseEnterWidget.RaiseMouseLeave()
                    End If
                    widget.RaiseMouseEnter()
                    MouseEnterWidget = widget
                Else ' mouse moved within the current MouseEnterWidget, no action needs to be taken
                End If
            Else
                If MouseEnterWidget IsNot Nothing Then
                    MouseEnterWidget.RaiseMouseLeave()
                    MouseEnterWidget = Nothing
                End If
            End If
        End If
    End Sub
    ' Stops browsing if initiated
    Private Sub SubMouseUp(sender As Object, e As MouseEventArgs) Handles Me.MouseUp
        If HoldFlag = True Then
            Return
        End If

        Me.Cursor = Cursors.Hand
        MouseDownFlag = False
    End Sub
    ' Responsible for firing widgets MouseLeave event
    Private Sub SubMouseLeave(sender As Object, e As EventArgs) Handles Me.MouseLeave
        If MouseEnterWidget IsNot Nothing Then
            MouseEnterWidget.RaiseMouseLeave()
            MouseEnterWidget = Nothing
        End If
    End Sub
    ' Responsible for rebuilding and repainting the sample picture appropriately to new size
    Private Sub SubSizeChanged(sender As Object, e As EventArgs) Handles Me.SizeChanged
        If MyBase.Image Is Nothing Then
            Return
        End If

        If SampleFitted IsNot Nothing Then
            SampleFitted.Dispose()
        End If

        ' Copy sample bitmap to a bitmap that has height and length adequate to the PanoramaBox dimensions. Note that:
        ' Height = Sample.Height * HeightRatio
        ' Length = Sample.Width * WidthRatio
        RebuildSampleFitted()
        RepaintBox()
    End Sub

    ' Stops panorama on the current crop and doesn't move it until released
    Public Sub Hold()
        HoldFlag = True
    End Sub

    ' Releases panorama so it is browseable again
    Public Sub Release()
        HoldFlag = False
    End Sub

    ' Shifts panoramic picture either backwards(negative value) or forwards(positive value) 
    Public Sub Shift(value As Integer)
        value = value Mod Length
        Dim newOffset = Offset + value

        If newOffset < 0 Then
            Offset = Length + newOffset
        ElseIf newOffset >= Length Then
            Offset = newOffset - Length
        Else
            Offset = newOffset
        End If
    End Sub

    Private Sub WidgetChanged()
        RebuildSampleFitted()
        RepaintBox()
    End Sub

    ' Paints passed widget on the SampleFitted bitmap
    Private Sub PaintWidget(widget As PanoramaBoxWidget)
        If widget.Width = 0 Or widget.Height = 0 Then
            Return
        End If

        Dim g As Graphics = Graphics.FromImage(SampleFitted)

        If widget.Left + widget.Width <= Length Then
            g.DrawImage(widget.Image, widget.Left, widget.Top, widget.Width, widget.Height)
        Else  ' widget.Left + widget.Width > Length
            Dim excess As Integer = widget.Left + widget.Width - Length

            Dim regRect As New Rectangle(0, 0, widget.Width - excess, widget.Height)
            Dim excessRect As New Rectangle(widget.Width - excess, 0, excess, widget.Height)

            Dim widgetSampleFitted As Bitmap = New Bitmap(widget.Image, New Size(widget.Width, widget.Height))
            Dim regPart As Bitmap = widgetSampleFitted.Clone(regRect, SampleFitted.PixelFormat)
            Dim excessPart As Bitmap = widgetSampleFitted.Clone(excessRect, SampleFitted.PixelFormat)

            g.DrawImage(regPart, widget.Left, widget.Top, widget.Width - excess, widget.Height)
            g.DrawImage(excessPart, 0, widget.Top, excess, widget.Height)

            widgetSampleFitted.Dispose()
            regPart.Dispose()
            excessPart.Dispose()
        End If
    End Sub

    ' Private list of widgets that belong to the PanoramaBox
    ' All operations on widgets must be made through appropriate function 
    Private Widgets As List(Of PanoramaBoxWidget) = New List(Of PanoramaBoxWidget)

    Public Sub AddWidget(widget As PanoramaBoxWidget)
        widget.PanoramaBox = Me
        Widgets.Add(widget)
        AddHandler widget.Changed, AddressOf WidgetChanged

        PaintWidget(widget)
        RepaintBox()
    End Sub
    Public Function GetWidget(index As Integer) As PanoramaBoxWidget
        Return Widgets(index)
    End Function
    Public Sub RemoveWidget(index As Integer)
        If Widgets(index) Is MouseEnterWidget Then
            MouseEnterWidget = Nothing
        End If

        Widgets(index).PanoramaBox = Nothing
        Widgets.RemoveAt(index)
        RebuildSampleFitted()
        RepaintBox()
    End Sub
    Public Sub RemoveWidget(widget As PanoramaBoxWidget)
        If widget Is MouseEnterWidget Then
            MouseEnterWidget = Nothing
        End If

        widget.PanoramaBox = Nothing
        Widgets.Remove(widget)
        RebuildSampleFitted()
        RepaintBox()
    End Sub
    ' Removes all widgets 
    Public Sub ClearWidgets()
        MouseEnterWidget = Nothing

        For Each widget As PanoramaBoxWidget In Widgets
            widget.PanoramaBox = Nothing
        Next

        Widgets.Clear()
        RepaintBox()
    End Sub
    ' Finds a widget that overlaps a point at (x,y) 
    Public Function GetWidget(x As Integer, y As Integer)
        ' Todo: Add exceptions when x or y are negative

        For Each widget As PanoramaBoxWidget In Widgets
            If widget.Left + widget.Width <= Length Then
                If widget.Left <= x And x <= widget.Left + widget.Width And widget.Top <= y And y <= widget.Top + widget.Height Then
                    Return widget
                End If
            Else ' widget.Left + widget.Width > Length
                If widget.Top <= y And y <= widget.Top + widget.Height Then
                    Dim excess As Integer = widget.Left + widget.Width - Length

                    If widget.Left <= x And x <= Length Then
                        Return widget
                    ElseIf x <= excess Then
                        Return widget
                    End If
                End If
            End If
        Next

        Return Nothing
    End Function
    Public Function GetCount() As Integer
        Return Widgets.Count
    End Function
End Class
