﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FreeView
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ViewToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.mbtnOpenFile = New System.Windows.Forms.ToolStripMenuItem()
        Me.mbtnExit = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.mbtnAbout = New System.Windows.Forms.ToolStripMenuItem()
        Me.mbtnClose = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ViewToolStripMenuItem, Me.HelpToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(539, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ViewToolStripMenuItem
        '
        Me.ViewToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mbtnOpenFile, Me.mbtnClose, Me.mbtnExit})
        Me.ViewToolStripMenuItem.Name = "ViewToolStripMenuItem"
        Me.ViewToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.ViewToolStripMenuItem.Text = "File"
        '
        'mbtnOpenFile
        '
        Me.mbtnOpenFile.Name = "mbtnOpenFile"
        Me.mbtnOpenFile.Size = New System.Drawing.Size(152, 22)
        Me.mbtnOpenFile.Text = "Open"
        '
        'mbtnExit
        '
        Me.mbtnExit.Name = "mbtnExit"
        Me.mbtnExit.Size = New System.Drawing.Size(152, 22)
        Me.mbtnExit.Text = "Exit"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mbtnAbout})
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.HelpToolStripMenuItem.Text = "Help"
        '
        'mbtnAbout
        '
        Me.mbtnAbout.Name = "mbtnAbout"
        Me.mbtnAbout.Size = New System.Drawing.Size(157, 22)
        Me.mbtnAbout.Text = "About FreeView"
        '
        'mbtnClose
        '
        Me.mbtnClose.Name = "mbtnClose"
        Me.mbtnClose.Size = New System.Drawing.Size(152, 22)
        Me.mbtnClose.Text = "Close"
        '
        'FreeView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(539, 409)
        Me.Controls.Add(Me.MenuStrip1)
        Me.KeyPreview = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "FreeView"
        Me.Text = "FreeView"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents ViewToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HelpToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mbtnOpenFile As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mbtnExit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mbtnAbout As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mbtnClose As System.Windows.Forms.ToolStripMenuItem

End Class
