﻿Imports System.Drawing.Imaging

Public Class ZoomWidget
    Inherits ActionWidget

    Public Sub New(left As Integer, top As Integer, width As Integer, height As Integer, Optional pBox As PanoramaBox = Nothing)
        MyBase.New(My.Resources.ZoomWidgetLeave, left, top, width, height, WidgetType.Zoom, pBox)
        SampleFocus = My.Resources.ZoomWidgetEnter
    End Sub

End Class
