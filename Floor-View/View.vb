﻿Imports System.IO
Imports System.Runtime.Serialization.Formatters.Binary
Imports Microsoft.VisualBasic.FileIO.FileSystem
Imports System.Text

' View is a class that basically consists of a panoramic picture and widgets assigned to that picture.
' The mentioned, are loaded in a PanoramaBox control creating a "view" that can be browsed and from which 
' a user is able to move to other views.
' 
' View class' basic purpose is to be saved to and loaded from a file, since it simply holds data meant to be displayed on PanoramaBox.
Public Class View
    Private mFilename As String = Nothing
    Private mName As String = Nothing
    Private mPanorama As Bitmap = Nothing
    Private mPanoramaCropSize As Double = 0.2

    Public Widgets As List(Of ActionWidget) = New List(Of ActionWidget)
    Private Const Header As String = "VIEW10"
    Public Shared ReadOnly Extension As String = "view"

    ' Sample picture for PanoramaBox
    Public Property Panorama As Bitmap
        Get
            Return New Bitmap(mPanorama)
        End Get
        Set(bmp As Bitmap)
            If mPanorama IsNot Nothing Then
                mPanorama.Dispose()
            End If

            mPanorama = New Bitmap(bmp)
        End Set
    End Property
    Public ReadOnly Property Filename As String
        Get
            Return mFilename
        End Get
    End Property
    Public ReadOnly Property Name As String
        Get
            Return Path.GetFileNameWithoutExtension(Filename)
        End Get
    End Property
    Public ReadOnly Property PanoramaCropSize As Double
        Get
            Return mPanoramaCropSize
        End Get
    End Property

    Sub New(filename As String)
        mFilename = filename
        Load()
    End Sub

    ' File Structure:
    ' ************************
    ' Header [LOC10]
    ' Name
    '
    ' Number of widgets [N] 
    '
    ' <Block of N widgets>
    '       Type
    '       Name
    '       TargetLocation
    '       TargetLocationLoadOffset
    '       DefaultWidth
    '       DefaultHeight
    '       DefaultTop
    '       DefaultLeft
    '
    ' PanoramaCropSize
    ' Panorama <jpeg image>
    ' ************************
    Sub Save()
        Try
            File.Delete(Filename)
        Catch ex As Exception
        End Try

        Dim stream As StreamWriter = New StreamWriter(File.Open(Filename, FileMode.Create))
        Dim writer As BinaryWriter = New BinaryWriter(stream.BaseStream)

        writer.Write(Header)
        writer.Write(Name)

        SaveWidgets(writer)

        writer.Write(PanoramaCropSize)
        Panorama.Save(writer.BaseStream, System.Drawing.Imaging.ImageFormat.Jpeg)
        stream.Close()
    End Sub


    Sub SaveWidgets(writer As BinaryWriter)
        writer.Write(Widgets.Count)

        For Each widget As ActionWidget In Widgets
            writer.Write(widget.Type)
            writer.Write(widget.Name)
            writer.Write(widget.TargetView)
            writer.Write(widget.TargetViewLoadOffset)
            writer.Write(widget.DefaultWidth)
            writer.Write(widget.DefaultHeight)
            writer.Write(widget.DefaultTop)
            writer.Write(widget.DefaultLeft)
        Next
    End Sub

    Public Sub Load()
        If File.Exists(Filename) = False Then
            Return
        End If

        Dim stream As StreamReader = New StreamReader(Filename)
        Dim reader As BinaryReader = New BinaryReader(stream.BaseStream)

        Dim header As String = reader.ReadString() ' TODO: check if header is correct
        reader.ReadString()

        LoadWidgets(reader)

        mPanoramaCropSize = reader.ReadDouble()

        Dim mem As New MemoryStream(reader.BaseStream.Length)
        reader.BaseStream.CopyTo(mem)
        Panorama = New Bitmap(mem)

        reader.Close()
    End Sub

    Private Sub LoadWidgets(reader As BinaryReader)
        Widgets.Clear()

        Dim widgetsCount As Integer = reader.ReadInt32(), i As Integer = 0

        While i < widgetsCount
            Dim widget As ActionWidget = Nothing
            Dim type As ActionWidget.WidgetType = reader.ReadInt32()

            Dim name As String, targetLocation As String
            Dim defaultWidth As Integer, defaultHeight As Integer, defaultTop As Integer, defaultLeft As Integer
            Dim locationLoadOffset As Double

            name = reader.ReadString()
            targetLocation = reader.ReadString()
            locationLoadOffset = reader.ReadDouble()

            defaultWidth = reader.ReadInt32()
            defaultHeight = reader.ReadInt32()
            defaultTop = reader.ReadInt32()
            defaultLeft = reader.ReadInt32()

            If type = ActionWidget.WidgetType.Passage Then
                widget = New PassageWidget(defaultLeft, defaultTop, defaultWidth, defaultHeight)
            ElseIf type = ActionWidget.WidgetType.Zoom Then
                widget = New ZoomWidget(defaultLeft, defaultTop, defaultWidth, defaultHeight)
            ElseIf type = ActionWidget.WidgetType.Power Then
                widget = New PowerWidget(defaultLeft, defaultTop, defaultWidth, defaultHeight)
            ElseIf type = ActionWidget.WidgetType.Retrn Then
                widget = New ReturnWidget(defaultLeft, defaultTop, defaultWidth, defaultHeight)
            End If

            widget.Name = name
            widget.TargetView = targetLocation
            widget.TargetViewLoadOffset = locationLoadOffset
            Widgets.Add(widget)
            i += 1
        End While
    End Sub

    ' Loads all necessary data from passed PanoramaBox argument
    Public Sub LoadDataFromPanoramaBox(pBox As PanoramaBox)
        Widgets = New List(Of ActionWidget)

        Dim i As Integer = 0
        Dim count As Integer = pBox.GetCount()
        While i < count
            Widgets.Add(pBox.GetWidget(i))
            i += 1
        End While

        Panorama = pBox.Sample
        mPanoramaCropSize = pBox.CropSize
    End Sub

    ' Load all data to passed PanoramaBox argument
    Public Sub LoadDataToPanoramaBox(pBox As PanoramaBox, Optional loadOffset As Double = 0)
        pBox.Sample = Panorama
        pBox.CropSize = PanoramaCropSize
        pBox.Offset = loadOffset * pBox.Length

        For Each widget As ActionWidget In Widgets
            pBox.AddWidget(widget)
        Next
    End Sub
End Class
