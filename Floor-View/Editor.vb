﻿Public Class Editor
    Protected RectTop As Integer, RectLeft As Integer, RectBottom As Integer, RectRight As Integer
    Public IsDrawing As Boolean = False
    Private Control As PassageWidget
    Private Anchor As Point

    Public Function StartDrawing(x As Integer, y As Integer) As PassageWidget
        'Control = New PassageWidget()
        Anchor = New Point(x, y)

        IsDrawing = True

        Return Control
    End Function

    Public Function UpdateDrawing(x As Integer, y As Integer) As PassageWidget
        If IsDrawing = False Then
            Return Nothing
        End If

        If x < Anchor.X Then
            Control.Left = x
            Control.Width = Anchor.X - x
        Else
            Control.Left = Anchor.X
            Control.Width = x - Anchor.X
        End If

        If y < Anchor.Y Then
            Control.Top = y
            Control.Height = Anchor.Y - y
        Else
            Control.Top = Anchor.Y
            Control.Height = y - Anchor.Y
        End If

        Return Control
        ' Debug.Write(Control.Width & " " & Control.Height & vbNewLine)
    End Function

    Public Function StopDrawing() As PassageWidget
        IsDrawing = False

        Return Control
    End Function
End Class
