﻿Imports System.IO

' Location class is a base class holding entrance information loaded in the application.
' 
' #Note: At the moment the class only holds information about first view that is loaded into the program.
'        In future the class should expand and keep track of all views that belong to their location
Public Class Location
    Public mFilename As String
    Public FirstViewName As String = ""
    Public ViewLoadOffset As Double = 0
    Public Const Header As String = "LOC10"
    Public Shared ReadOnly Extension As String = "loc"

    Public ReadOnly Property Filename As String
        Get
            Return mFilename
        End Get
    End Property
    Public ReadOnly Property Name As String
        Get
            Return Path.GetFileNameWithoutExtension(Filename)
        End Get
    End Property

    Sub New(filename As String)
        mFilename = filename
    End Sub

    Public Sub Save()
        Try
            File.Delete(Filename)
        Catch ex As Exception
        End Try

        Dim stream As StreamWriter = New StreamWriter(File.Open(Filename, FileMode.Create))
        Dim writer As BinaryWriter = New BinaryWriter(stream.BaseStream)

        writer.Write(Header)
        writer.Write(Name)
        writer.Write(FirstViewName)
        writer.Write(ViewLoadOffset)

        stream.Close()
    End Sub

    Public Sub Load()
        If File.Exists(Filename) = False Then
            Return
        End If

        Dim stream As StreamReader = New StreamReader(Filename)
        Dim reader As BinaryReader = New BinaryReader(stream.BaseStream)

        Dim header As String = reader.ReadString() ' TODO: check if header is correct
        reader.ReadString() ' name
        FirstViewName = reader.ReadString()
        ViewLoadOffset = reader.ReadDouble()

        reader.Close()
    End Sub
End Class
