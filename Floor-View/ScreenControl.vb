﻿Public Class ScreenControl
    ' Class which controls the screen size the user is using. It can either be full screen or normal.
    Shared fullscreen As Boolean = False
    ''' <summary>
    '''Function which toggles Full/Normal Screen
    '''</summary>
    ''' <example>ScreenControl.maxScreen(False/True)</example>
    ''' <history>[Adrian Iwaszkiewicz] 23/2/2015</history>
    Public Shared Sub maxScreen(onOff As Boolean)
        ' If to fullscreen...
        If onOff Then
            'Change appearance of the window.
            FreeView.FormBorderStyle = Windows.Forms.FormBorderStyle.None
            FreeView.WindowState = FormWindowState.Maximized
            fullscreen = True
            ' Exit fullscreen.
        Else
            'Change the appearance of the window.
            FreeView.FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable
            FreeView.WindowState = FormWindowState.Normal
            fullscreen = False
        End If

    End Sub

    ''' <summary>Check in which state the window currently is.</summary>
    ''' <returns>Returns True/False, True = FullScreen, False = Normal Screen.</returns>
    ''' <example>If ScreenControl.isFullScreen() Then ....</example>
    ''' <history>[Adrian Iwaszkiewicz] 23/2/2015</history>
    Public Shared Function isFullScreen()
        'returns a boolean.
        Return fullscreen
    End Function

End Class
