﻿Imports System.IO


' The main FreeView application's design is very simple.
' The form consists of a menu strip on the top and the rest
' of its space is occupied by a PanoramaBox control.
' After loading a new .loc file subsequent views are displayed in the PanoramaBox 
' as a user browses the location.
Public Class FreeView
    Public CurrentLocation As Location = Nothing
    Public CurrentView As View = Nothing
    Private PanoramaBox As PanoramaBox = New PanoramaBox()

    ' Setup and add the application's PanoramaBox
    Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        PanoramaBox.Top = 0
        PanoramaBox.Left = 0
        PanoramaBox.Size = Me.ClientSize
        Controls.Add(PanoramaBox)
        AddHandler PanoramaBox.WidgetClicked, AddressOf Me.WidgetClicked

        Clear()
    End Sub

    ' Sets the application to an initial state
    Private Sub Clear()
        PanoramaBox.Sample = My.Resources.noimage
        PanoramaBox.CropSize = 1
        CurrentLocation = Nothing
        CurrentView = Nothing
        mbtnClose.Enabled = False
    End Sub

    ' Resize the application's PanoramaBox to fill its form
    Private Sub Form_Resize(sender As Object, e As EventArgs) Handles MyBase.Resize
        PanoramaBox.Width = Me.ClientSize.Width
        PanoramaBox.Height = Me.ClientSize.Height
    End Sub

    ' Open a FreeView Location file 
    Private Sub mbtnOpenFile_Click(sender As Object, e As EventArgs) Handles mbtnOpenFile.Click
        Dim ofd As New OpenFileDialog()
        ofd.Title = "Open View"
        ofd.Filter = "FreeView Location Files (*.loc) | *.loc"

        If ofd.ShowDialog() <> Windows.Forms.DialogResult.OK Then
            Return
        End If

        LoadLocation(ofd.FileName)
        If CurrentLocation IsNot Nothing Then
            mbtnClose.Enabled = True
        End If
    End Sub

    ' Setup and load a view into the application
    Public Sub LoadLocation(filename As String)
        Try
            CurrentLocation = New Location(filename)
            CurrentLocation.Load()
        Catch ex As Exception
            MsgBox("The location file seems to be corrupted and cannot be open.", MsgBoxStyle.OkOnly, "Error")
            Clear()
            Return
        End Try

        LoadView(CurrentLocation.FirstViewName, CurrentLocation.ViewLoadOffset)
    End Sub

    ' Setup and load a location into the aplication's PanoramaBox
    Public Sub LoadView(name As String, loadOffset As Double)
        Try
            CurrentView = New View(ViewNameToFilename(name))
            CurrentView.LoadDataToPanoramaBox(PanoramaBox, loadOffset)
        Catch ex As Exception
            Clear()
            MsgBox("There was a problem with opening new view.", MsgBoxStyle.OkOnly, "Error")
            Return
        End Try
    End Sub

    ' Load location being hold by a clicked widget
    Public Sub WidgetClicked(widget As PanoramaBoxWidget)
        Dim actionWidget As ActionWidget = widget
        LoadView(actionWidget.TargetView, actionWidget.TargetViewLoadOffset)
    End Sub

    ' Exit application
    Private Sub mbtnExit_Click(sender As Object, e As EventArgs) Handles mbtnExit.Click
        Close()
    End Sub

    ' Show about
    Private Sub mbtnAbout_Click(sender As Object, e As EventArgs) Handles mbtnAbout.Click
        frmAbout.Show()
    End Sub

    Private Function ViewNameToFilename(name As String) As String
        Return Path.GetDirectoryName(CurrentLocation.Filename) & "\" & name & "." & View.Extension
    End Function

    Private Sub mbtnClose_Click(sender As Object, e As EventArgs) Handles mbtnClose.Click
        Clear()
    End Sub
End Class