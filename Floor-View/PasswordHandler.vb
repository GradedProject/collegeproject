﻿Imports System.Security.Policy
Imports System.Security.Cryptography
Imports System.Text
''' <summary>
''' Class used to handle Encrypting passwords to SHA 256 (256 bit) standards.
''' Use of 256 Bit encryption ensures that the password will not get cracked. 
''' Used to enter the Editors mode securely.
''' </summary>
''' <history>[Adrian Iwaszkiewicz] 10.3.2015</history>
''' <remarks></remarks>
Public Class PasswordHandler

    Public Shared Function Encrypt(ByVal ClearString As String) As String
        'Define new unicode encoding variable.
        Dim uEncode As New UnicodeEncoding()
        'Define a new byte used to create our hash
        Dim bytClearString() As Byte = uEncode.GetBytes(ClearString)
        'Define the 265bit SHA encryption (import it)
        Dim sha As New  _
        System.Security.Cryptography.SHA256Managed()
        'Create a hash
        Dim hash() As Byte = sha.ComputeHash(bytClearString)
        'Convert the hash to string
        Return Convert.ToBase64String(hash)
        'Print hash for debug purposes.
        'System.Console.Write(Convert.ToBase64String(hash))
    End Function
    'Functions purpose is to Verify two hashes by comparing both. 
    'It takes in a value of a password the user has entered in InputBox
    Public Shared Function VerifyPassword(ByVal inPassword As String) As String
        Return True
        'Define a new password string and load a saved file with password hash
        Dim password As String = My.Computer.FileSystem.ReadAllText(Environment.GetFolderPath((Environment.SpecialFolder.Desktop)) + "/FloorView/password.txt")
        'Convert the password user has entered into 265 bit hash, then compare them
        If password.Equals(Encrypt(inPassword)) Then
            'Both hashes match, returning true
            Return True
        Else
            'The  hashes are different, returning false
            Return False
        End If
    End Function
End Class
