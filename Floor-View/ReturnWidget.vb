﻿Imports System.Drawing.Imaging

Public Class ReturnWidget
    Inherits ActionWidget

    Public Sub New(left As Integer, top As Integer, width As Integer, height As Integer, Optional pBox As PanoramaBox = Nothing)
        MyBase.New(My.Resources.ReturnWidgetLeave, left, top, width, height, WidgetType.Retrn, pBox)
        SampleFocus = My.Resources.ReturnWidgetEnter
    End Sub

End Class
