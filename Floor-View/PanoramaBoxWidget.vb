﻿' PanoramaBoxWidget is a base class for controls that are meant
' to be placed on PanoramaBox objects.
' Widgets created from this class differ from basic Windows Forms controls - instead of being
' assigned to their owner's box, they are assigned to their owner's panoramic picture.
' This means that when the panoramic picture is browsed, widget controls shift as well
' while windows' controls remain in the same static position.
Public Class PanoramaBoxWidget
    Public PanoramaBox As PanoramaBox
    Private mSample As Bitmap = Nothing
    Private mSampleFocus As Bitmap = Nothing

    ' Set true when the widget gets attention
    ' At the moment it's the case only when the mouse is above the widget
    Private FocusFlag As Boolean = False
    ' Set true in constructor when the widget is just being
    ' built to prevent raising the "Changed" event
    Private InitFlag As Boolean

    ' Sample bitmap for the widget
    Public Property Sample As Bitmap
        Get
            Return mSample
        End Get
        Set(bmp As Bitmap)
            If mSample IsNot Nothing Then
                mSample.Dispose()
            End If

            mSample = bmp
            RaiseChanged()
        End Set
    End Property

    ' Sample bitmap for the widget when the control gets focus.
    ' If not set, it's the same bitmap as the Sample bitmap
    Public Property SampleFocus As Bitmap
        Get
            If mSampleFocus IsNot Nothing Then
                Return mSampleFocus
            Else
                Return mSample
            End If
        End Get
        Set(bmp As Bitmap)
            If mSampleFocus IsNot Nothing Then
                mSampleFocus.Dispose()
            End If

            mSampleFocus = bmp
            RaiseChanged()
        End Set
    End Property

    ' Returns the current bitmap that should be displayed for the widget,
    ' it's either Sample bitmap or SampleFocus bitmap
    Public ReadOnly Property Image
        Get
            If FocusFlag = True Then
                Return SampleFocus
            Else
                Return Sample
            End If
        End Get
    End Property

    Private mDefaultTop As Integer = 0
    Private mDefaultLeft As Integer = 0
    Private mDefaultHeight As Integer = 0
    Private mDefaultWidth As Integer = 0

    ' Default dimensions are the values for the widget
    ' when its PanoramaBox WidthRatio and HeightRatio equal 1
    Public Property DefaultTop As Integer
        Get
            Return mDefaultTop
        End Get
        Set(defTop As Integer)
            mDefaultTop = defTop
            RaiseChanged()
        End Set
    End Property
    Public Property DefaultLeft As Integer
        Get
            Return mDefaultLeft
        End Get
        Set(defLeft As Integer)
            mDefaultLeft = defLeft
            RaiseChanged()
        End Set
    End Property
    Public Property DefaultHeight As Integer
        Get
            Return mDefaultHeight
        End Get
        Set(defHeight As Integer)
            mDefaultHeight = defHeight
            RaiseChanged()
        End Set
    End Property
    Public Property DefaultWidth As Integer
        Get
            Return mDefaultWidth
        End Get
        Set(defWidth As Integer)
            mDefaultWidth = defWidth
            RaiseChanged()
        End Set
    End Property

    ' Current widget dimensions relevant to its PanoramaBox ratios
    Public Property Top As Integer
        Get
            If PanoramaBox IsNot Nothing Then
                Return DefaultTop * PanoramaBox.HeightRatio
            Else
                Return DefaultTop
            End If
        End Get
        Set(top As Integer)
            If PanoramaBox IsNot Nothing Then
                DefaultTop = top / PanoramaBox.HeightRatio
            Else
                DefaultTop = top
            End If
        End Set
    End Property
    Public Property Left As Integer
        Get
            If PanoramaBox IsNot Nothing Then
                Return DefaultLeft * PanoramaBox.WidthRatio
            Else
                Return DefaultLeft
            End If
        End Get
        Set(left As Integer)
            If PanoramaBox IsNot Nothing Then
                DefaultLeft = left / PanoramaBox.WidthRatio
            Else
                DefaultLeft = left
            End If
        End Set
    End Property
    Public Property Height As Integer
        Get
            If PanoramaBox IsNot Nothing Then
                Return DefaultHeight * PanoramaBox.HeightRatio
            Else
                Return DefaultHeight
            End If
        End Get
        Set(height As Integer)
            If PanoramaBox IsNot Nothing Then
                DefaultHeight = height / PanoramaBox.HeightRatio
            Else
                DefaultHeight = height
            End If
        End Set
    End Property
    Public Property Width As Integer
        Get
            If PanoramaBox IsNot Nothing Then
                Return DefaultWidth * PanoramaBox.WidthRatio
            Else
                Return DefaultWidth
            End If
        End Get
        Set(width As Integer)
            If PanoramaBox IsNot Nothing Then
                DefaultWidth = width / PanoramaBox.WidthRatio
            Else
                DefaultWidth = width
            End If
        End Set
    End Property

    Public Sub SetPos(top As Integer, left As Integer, height As Integer, width As Integer)
        If PanoramaBox Is Nothing Then
            mDefaultTop = top
            mDefaultLeft = left
            mDefaultHeight = height
            mDefaultWidth = width
        Else
            mDefaultTop = top / PanoramaBox.HeightRatio
            mDefaultLeft = left / PanoramaBox.WidthRatio
            mDefaultHeight = height / PanoramaBox.HeightRatio
            mDefaultWidth = width / PanoramaBox.WidthRatio
        End If

        RaiseChanged()
    End Sub

    Public Event MouseEnter()
    Public Event MouseLeave()
    Public Event MouseDown()
    Public Event Changed()

    Public Sub RaiseMouseEnter()
        RaiseEvent MouseEnter()
    End Sub
    Public Sub RaiseMouseLeave()
        RaiseEvent MouseLeave()
    End Sub
    Public Sub RaiseMouseDown()
        RaiseEvent MouseDown()
    End Sub
    Protected Sub RaiseChanged()
        If InitFlag = False Then
            RaiseEvent Changed()
        End If
    End Sub

    Public Sub New(sample As Bitmap, left As Integer, top As Integer, width As Integer, height As Integer, Optional pBox As PanoramaBox = Nothing)
        InitFlag = True

        PanoramaBox = pBox
        Me.Sample = sample
        Me.Top = top
        Me.Left = left
        Me.Height = height
        Me.Width = width

        InitFlag = False

        If pBox IsNot Nothing Then
            PanoramaBox.AddWidget(Me)
        End If
    End Sub

    Protected Sub SubMouseEnter() Handles Me.MouseEnter
        FocusFlag = True
        RaiseChanged()
    End Sub

    Protected Sub SubMouseLeave() Handles Me.MouseLeave
        FocusFlag = False
        RaiseChanged()
    End Sub

    ' Destructor
    Sub Dispose()
        Sample.Dispose()
        SampleFocus.Dispose()
    End Sub
End Class
