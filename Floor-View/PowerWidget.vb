﻿Imports System.Drawing.Imaging

Public Class PowerWidget
    Inherits ActionWidget

    Public Sub New(left As Integer, top As Integer, width As Integer, height As Integer, Optional pBox As PanoramaBox = Nothing)
        MyBase.New(My.Resources.PowerWidgetLeave, left, top, width, height, WidgetType.Power, pBox)
        SampleFocus = My.Resources.PowerWidgetEnter
    End Sub

End Class
